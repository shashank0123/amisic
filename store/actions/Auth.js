import {
  LOGIN_SUCCESS,
  LOGOUT,
  LOGIN_ERROR,
  CLEARE_ALL,
  LOADING,
  REGISTER_SUCCESS,
  REGISTER_FAIL,
  LOADING_FALSE,
} from './types';
import auth from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore';
import storage from '@react-native-firebase/storage';
import {Alert} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import {getPosts} from './Home';

// const auth = auth();
// const firestore = firestore();
// const storage = firebase.storage();

export const login = (credential, navigation) => async dispatch => {
  dispatch({type: LOADING});
  try {
    const res = await auth().signInWithEmailAndPassword(
      credential.email.toLowerCase(),
      credential.password.toLowerCase(),
    );

    //console.log('res', res);

    const data = await firestore()
      .collection('users')
      .where('userId', '==', res.user.uid)
      .get();
    console.log('data', data);
    if (data.docs.length) {
      for (snap of data.docs) {
        let userProfile = snap.data();
        userProfile.key = snap.id;

        dispatch({type: LOGIN_SUCCESS, payload: userProfile});
        dispatch(getPosts(navigation));
        await AsyncStorage.setItem('user', JSON.stringify(userProfile));
        dispatch({type: LOADING_FALSE});
        //setTimeout(() => navigation.navigate('Home'), 3000);
      }
    }
  } catch (err) {
    dispatch({
      type: LOGIN_ERROR,
    });
    Alert.alert('Error', err.message);
  }
};

export const onAuthStateChange = navigation => async dispatch => {
  auth().onAuthStateChanged(async user => {
    if (user) {
      const data = await firestore()
        .collection('users')
        .where('userId', '==', user.uid)
        .get();
      if (data.docs.length) {
        for (snap of data.docs) {
          let userProfile = snap.data();
          userProfile.key = snap.id;

          dispatch({type: LOGIN_SUCCESS, payload: userProfile});
          dispatch(getPosts(navigation));
          //setTimeout(() => navigation.navigate('Home'), 3000);
        }
      }
    }
  });
};

export const createAccount = (credential, navigation) => async dispatch => {
  dispatch({type: LOADING});
  try {
    console.log('comes to createaccount');
    console.log('credential', credential);
    const res = await auth().createUserWithEmailAndPassword(
      credential.email.toLowerCase(),
      credential.password.toLowerCase(),
    );
    console.log('createuserres', res);

    dispatch({type: LOADING_FALSE});

    navigation.navigate('Login');

    const uploadingData = {
      ...credential,
      userId: res.user.uid,
    };

    await firestore()
      .collection('users')
      .doc(res.user.uid)
      .set(uploadingData);

    dispatch({type: LOADING_FALSE});
    // const data = await firestore
    //   .collection('users')
    //   .where('userId', '==', res.user.uid)
    //   .get();

    // for (user of data.docs) {
    //   let userProfile = user.data();
    //   userProfile.key = user.id;

    //   dispatch({type: REGISTER_SUCCESS, payload: userProfile});
    //   await AsyncStorage.setItem('user', JSON.stringify(userProfile));
    //   navigation.navigate('Home');
    // }
  } catch (err) {
    dispatch({type: REGISTER_FAIL});
    dispatch({type: LOADING_FALSE});
    Alert.alert('Error', err.message);
  }
};

export const getCurrentUser = () => async dispatch => {
  const currentUser = auth().currentUser.uid;

  try {
    const user = await firestore()
      .collection('users')
      .where('userId', '==', currentUser)
      .get();
    for (snap of user.docs) {
      const data = snap.data();
      data.key = snap.id;
      dispatch({type: LOGIN_SUCCESS, payload: data});
      dispatch({type: LOADING_FALSE});
    }
  } catch (err) {
    console.log(err.message);
  }
};

export const updateProfile = (
  data,
  key,
  saveProfile,
  userId,
  profile,
  clearState,
) => async dispatch => {
  dispatch({type: LOADING});

  const currentUser = auth().currentUser.uid;

  if (profile && profile.path) {
    const ext = profile.path.split('.').pop(); // Extract image extension
    const filename = `${profile.fileName}.${currentUser}.${ext}`; // Generate unique name

    try {
      if (profile.uri) {
        storage()
          .ref(`images/${filename}`)
          .putFile(profile.path)
          .then(async snapshot => {
            const myUrl = await storage()
              .ref(`images/${filename}`)
              .getDownloadURL();
            await firestore()
              .collection('users')
              .doc(key)
              .update({...data, profile: myUrl});
            dispatch(getCurrentUser());
            dispatch({type: LOADING_FALSE});

            //clearState();
          });
      } else {
        await firestore()
          .collection('users')
          .doc(key)
          .update(data);
        // navigation.navigate('Profile');
        dispatch(getCurrentUser());
        dispatch({type: LOADING_FALSE});

        // const doc = await firestore
        //   .collection('users')
        //   .where('userId', '==', userId)
        //   .get();

        // for (user of doc.docs) {
        //   let userProfile = user.data();
        //   userProfile.key = user.id;

        //   dispatch({type: LOGIN_SUCCESS, payload: userProfile});
        //   saveProfile();
        // }
      }
    } catch (err) {
      Alert.alert('Error!', err.message);
    }
  } else {
    await firestore()
      .collection('users')
      .doc(key)
      .update(data);
    dispatch(getCurrentUser());
    dispatch({type: LOADING_FALSE});
  }
};

export const autoAuth = data => dispatch => {
  dispatch({type: LOGIN_SUCCESS, payload: data});
};

export const logout = navigation => async dispatch => {
  await auth().signOut();
  await AsyncStorage.removeItem('user');
  dispatch({type: LOGOUT});
  //dispatch({type: CLEARE_ALL});
  navigation.navigate('Login');
};

export const forgetPassword = email => async dispatch => {
  dispatch({type: LOADING});
  try {
    await auth().sendPasswordResetEmail(email);
    Alert.alert(`Success','Please check your email at ${email}`);
    dispatch({type: LOGIN_SUCCESS});
  } catch (err) {
    Alert.alert('Error', err.message);
    dispatch({type: LOADING_FALSE});
  }
};

export const changePassword = (oldPass, newPass) => async dispatch => {
  dispatch({type: LOADING});
  const currentUser = auth().currentUser;
  const user = await firestore()
    .collection('users')
    .where('userId', '==', currentUser.uid)
    .get();

  try {
    for (const doc of user.docs) {
      if (oldPass !== doc.data().password) {
        dispatch({type: LOADING_FALSE});
        return Alert.alert(
          'Inavalid',
          'oldpassowrd does not match with current password ',
        );
      }
      await currentUser.updatePassword(newPass);

      dispatch({type: LOADING_FALSE});

      Alert.alert('Success', 'succeffully changed password');
    }
  } catch (err) {
    Alert.alert('Error', err.message);
    dispatch({type: LOADING_FALSE});
  }
};

export const clearAll = () => dispatch => {
  dispatch({type: CLEARE_ALL});
};

export const clearLoader = () => dispatch => {
  dispatch({type: LOADING_FALSE});
};
