import auth from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore';
import {LOADING, GET_FRIENDS_SUCCESS, LOADING_FALSE} from './types';
import {Alert} from 'react-native';

// const firestore = firestore();

export const getUnique = (arr, comp) => {
  // store the comparison  values in array
  const unique = arr
    .map(e => e[comp])

    // store the indexes of the unique objects
    .map((e, i, final) => final.indexOf(e) === i && i)

    // eliminate the false indexes & return unique objects
    .filter(e => arr[e])
    .map(e => arr[e]);

  return unique;
};

export const getAllFriendsSuggestion = () => async dispatch => {
  dispatch({type: LOADING});
  const uid = auth().currentUser.uid;
  try {
    const rangeArray = [];
    const allFriends = [];
    const myCircle = await firestore()
      .collection('myCircle')
      .where('userId', '==', uid)
      .get();

    // const allUser = await firestore.collection('users').get();

    if (myCircle.docs.length) {
      for (snap of myCircle.docs) {
        rangeArray.push(snap.data().range);
      }

      if (rangeArray.length === myCircle.docs.length) {
        rangeArray.map(range => {
          firestore()
            .collection('myCircle')
            .where('range', '<=', range)
            .get()
            .then(snapshot => {
              snapshot.docs.map(data => {
                if (data.data().userId !== uid) {
                  const myData = data.data();
                  myData.key = data.id;
                  firestore()
                    .collection('users')
                    .where('userId', '==', myData.userId)
                    .get()
                    .then(snapshot1 => {
                      snapshot1.docs.map(user => {
                        console.log(user.data().userId);
                        const friends = user.data();
                        friends.key = user.id;
                        allFriends.push(friends);
                      });

                      if (allFriends.length) {
                        dispatch({
                          type: GET_FRIENDS_SUCCESS,
                          payload: getUnique(allFriends, 'userId'),
                        });
                      }
                    });
                }
              });
            });
        });
      }
    }

    // const friendsArray = [];

    // if (user.docs.length) {
    //   for (snap of user.docs) {
    //     let friends = snap.data();
    //     friends.key = snap.id;
    //     friendsArray.push(friends);
    //   }
    // }

    // if (user.docs.length === friendsArray.length) {
    //   friendsArray.map(val => {
    //     console.log(val.under);
    //     firestore
    //       .collection('users')
    //       .where('userId', '==', val.under)
    //       .get()
    //       .then(snapshot => {
    //         snapshot.docs.map(res => {
    //           const data = res.data();
    //           data.key = res.id;
    //           allFriends.push(data);
    //         });

    //         if (snapshot.docs.length === allFriends.length) {
    //           dispatch({
    //             type: GET_FRIENDS_SUCCESS,
    //             payload: allFriends,
    //           });
    //           dispatch({type: LOADING_FALSE});
    //         }
    //       });
    //   });
    // }
  } catch (err) {
    dispatch({type: LOADING_FALSE});
    Alert.alert('Error', err.message);
  }
};
