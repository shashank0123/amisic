import {
  LOADING,
  LOADING_FALSE,
  GET_CURRENT_USER_GROUPS,
  GET_CURRENT_USERS_CREATED_GROUPS,
  GET_CIRCLE_UNDER_RANGE,
  GET_CIRCLE_SUGGESTIONS,
  GET_GROUP_POSTS,
} from './types';

import auth from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore';
import storage from '@react-native-firebase/storage';
import {Alert} from 'react-native';
import {getUserDetailsById} from '../../utils';
// const firestore = firestore();
// const storage = firebase.storage();

export const getUnique = (arr, comp) => {
  // store the comparison  values in array
  const unique = arr
    .map(e => e[comp])

    // store the indexes of the unique objects
    .map((e, i, final) => final.indexOf(e) === i && i)

    // eliminate the false indexes & return unique objects
    .filter(e => arr[e])
    .map(e => arr[e]);

  return unique;
};

// export const getGroupPosts = circleId => async dispatch => {
//   try {
//     console.log('circleId', circleId);
//     const posts = await firestore
//       .collection('posts')
//       .where('circleId', '==', circleId)
//       .orderBy('createdAt', 'DESC')
//       .limit(20)
//       .get();

//     console.log('posts', posts);
//     const postsArray = [];
//     if (posts.docs.length) {
//       for (snap of posts.docs) {
//         let posts = snap.data();
//         posts.key = snap.id;
//         postsArray.push(posts);
//       }
//       if (posts.docs.length === postsArray.length) {
//         dispatch({type: GET_GROUP_POSTS, payload: postsArray});
//         dispatch({type: LOADING_FALSE});
//       }
//     }
//   } catch (err) {
//     dispatch({type: LOADING_FALSE});
//     console.log('err', err);
//   }
// };

export const getCurrentUsersCreatedGroups = () => async dispatch => {
  //console.log('comes here...........');
  const currentUser = auth().currentUser;
  try {
    const groups = await firestore()
      .collection('circles')
      .where('creatorId', '==', currentUser.uid)

      //.orderBy('createdAt')
      .get();

    const groupArray = [];

    if (groups.docs.length) {
      for (snap of groups.docs) {
        let groups = snap.data();
        groups.key = snap.id;

        groupArray.push(groups);

        //navigation.navigate('Home');
      }

      if (groups.docs.length === groupArray.length) {
        dispatch({type: GET_CURRENT_USERS_CREATED_GROUPS, payload: groupArray});
        dispatch({type: LOADING_FALSE});
      }
    }
  } catch (err) {
    dispatch({type: LOADING_FALSE});
    Alert.alert('Error', err.message);
  }
};

export const getCurrentUserGroup = () => async dispatch => {
  const currentUser = auth().currentUser;
  try {
    const groups = await firestore()
      .collection('circles')
      .where('userIds', 'array-contains', currentUser.uid)

      //.orderBy('createdAt')
      .get();

    const groupArray = [];

    if (groups.docs.length) {
      for (snap of groups.docs) {
        let groups = snap.data();
        groups.key = snap.id;

        groupArray.push(groups);

        //navigation.navigate('Home');
      }

      if (groups.docs.length === groupArray.length) {
        dispatch({type: GET_CURRENT_USER_GROUPS, payload: groupArray});
        dispatch({type: LOADING_FALSE});
      }
    }
  } catch (err) {
    dispatch({type: LOADING_FALSE});
    Alert.alert('Error', err.message);
  }
};

export const createCircle = (data, file, clearState) => async dispatch => {
  const currentUser = auth().currentUser.uid;
  dispatch({type: LOADING});

  const ext = file.path.split('.').pop(); // Extract image extension

  const filename = `${file.fileName}.${currentUser}.${ext}`; // Generate unique name
  try {
    storage()
      .ref(`images/${filename}`)
      .putFile(file.path)
      .then(async snapshot => {
        //console.log('save snapshot', snapshot);
        const myUrl = await storage()
          .ref(`images/${filename}`)
          .getDownloadURL();
        const circleRef = firestore()
          .collection('circles')
          .doc();
        await circleRef.set({
          ...data,
          id: circleRef.id,
          profile: myUrl,
          createdAt: Date.now(),
        });
        clearState();
        dispatch({type: LOADING_FALSE});
        dispatch(getCurrentUserGroup());
      });
  } catch (err) {
    dispatch({type: LOADING_FALSE});
    Alert.alert('Error', err.message);
  }
};

export const getCircleSuggestions = () => async dispatch => {
  //console.log('comes int getCircleSuggestions');
  dispatch({type: LOADING});
  try {
    let suggestions = [];
    const myDetails = await getUserDetailsById(auth().currentUser.uid);
    const {userDisabilities, userId} = myDetails.data;
    //console.log('myDetails', myDetails);
    userDisabilities.forEach(async userDisability => {
      //console.log('userDisability', userDisability);
      const circleRes = await firestore()
        .collection('circles')
        .where('disabilityNames', 'array-contains', userDisability.name)
        .get();

      //console.log('circleRes', circleRes);

      if (!circleRes.empty) {
        circleRes._docs.forEach(doc => {
          const {userIds} = doc.data();
          if (!userIds.includes(userId)) {
            suggestions.push(doc.data());
          }
        });
        suggestions = [...new Set(suggestions)];
        dispatch({
          type: GET_CIRCLE_SUGGESTIONS,
          payload: suggestions,
        });
        //console.log('suggestions', suggestions);
      }
    });
  } catch (err) {
    console.log('err', err);
    dispatch({type: LOADING_FALSE});
  }
};

export const getCircleUnderRange = () => async dispatch => {
  dispatch({type: LOADING});

  const uid = auth().currentUser.uid;
  const allCircles = [];
  const allCirclesArray = [];
  const rangeArray = [];

  try {
    const myCircle = await firestore()
      .collection('myCircle')
      .where('userId', '==', uid)
      .get();

    if (myCircle.docs.length) {
      for (snap of myCircle.docs) {
        rangeArray.push(snap.data().range);
      }

      if (rangeArray.length === myCircle.docs.length) {
        rangeArray.map(range => {
          firestore()
            .collection('myCircle')
            .where('range', '<=', range)
            .get()
            .then(snapshot => {
              snapshot.docs.map(data => {
                console.log(snapshot.docs.length);
                if (data.data().userId !== uid) {
                  console.log(data.data());

                  const myData = data.data();
                  myData.key = data.id;
                  allCirclesArray.push(myData);
                }
              });

              if (allCirclesArray.length) {
                dispatch({
                  type: GET_CIRCLE_UNDER_RANGE,
                  payload: getUnique(allCirclesArray, 'key'),
                });
              }
            });
        });
      }
    } else {
      let circleWithoutFilter = await firestore()
        .collection('myCircle')
        .where('range', '<=', 10)
        .get();

      if (circleWithoutFilter.docs.length) {
        for (snap of circleWithoutFilter.docs) {
          const data = snap.data();
          data.key = snap.id;
          allCircles.push(data);
        }
        if (circleWithoutFilter.docs.length === allCircles.length) {
          dispatch({type: GET_CIRCLE_UNDER_RANGE, payload: allCircles});
        }
      }
    }

    // const circle = await firestore
    //   .collection('circle')
    //   .where('under', '==', uid)
    //   .get();
    // if (circle.docs.length) {
    //   for (snap of circle.docs) {
    //     const data = snap.data();
    //     data.key = snap.id;
    //     allCircles.push(data);
    //   }
    //   if (circle.docs.length === allCircles.length) {
    //     dispatch({type: GET_CIRCLE_UNDER_RANGE, payload: allCircles});
    //   }
    // } else {
    //   let circleWithoutFilter = await firestore
    //     .collection('myCircle')
    //     .where('range', '<=', 10)
    //     .get();

    //   if (circleWithoutFilter.docs.length) {
    //     for (snap of circleWithoutFilter.docs) {
    //       const data = snap.data();
    //       data.key = snap.id;
    //       allCircles.push(data);
    //     }
    //     if (circleWithoutFilter.docs.length === allCircles.length) {
    //       dispatch({type: GET_CIRCLE_UNDER_RANGE, payload: allCircles});
    //     }
    //   }
    // }
  } catch (err) {
    dispatch({type: LOADING_FALSE});
    Alert.alert('Error', err.message);
  }
};

export const deleteCircle = key => async dispatch => {
  try {
    await firestore()
      .collection('circles')
      .doc(key)
      .delete();
    dispatch(getCurrentUserGroup());
    dispatch(getCurrentUsersCreatedGroups());
    dispatch({type: LOADING_FALSE});
  } catch (err) {
    Alert.alert('Error', err.message);
    dispatch({type: LOADING_FALSE});
  }
};

export const updateCircle = (key, data, closeModal, file) => async dispatch => {
  dispatch({type: LOADING});
  const currentUser = auth().currentUser.uid;

  try {
    if (!file || !file.path) {
      await firestore()
        .collection('circles')
        .doc(key)
        .update(data);
      dispatch(getCurrentUserGroup());
      dispatch(getCurrentUsersCreatedGroups());
      dispatch({type: LOADING_FALSE});
      closeModal();
    } else {
      const ext = file.path.split('.').pop(); // Extract image extension

      const filename = `${file.fileName}.${currentUser}.${ext}`; // Generate unique name
      storage()
        .ref(`images/${filename}`)
        .putFile(file.path)
        .then(async snapshot => {
          const myUrl = await storage()
            .ref(`images/${filename}`)
            .getDownloadURL();
          await firestore()
            .collection('circles')
            .doc(key)
            .update({...data, profile: myUrl});
          dispatch(getCurrentUserGroup());
          dispatch(getCurrentUsersCreatedGroups());
          dispatch({type: LOADING_FALSE});
          closeModal();
        });
    }
  } catch (err) {
    Alert.alert('Error', err.message);
    console.log('errrrr', err);
    dispatch({type: LOADING_FALSE});
  }
};

export const clearAll = () => dispatch => {
  dispatch({type: LOADING_FALSE});
};
