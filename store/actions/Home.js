import {USER_POSTS} from './types';
import firestore from '@react-native-firebase/firestore';
import {getUserDetailsById} from '../../utils';
import auth from '@react-native-firebase/auth';
export const getPosts = navigation => async dispatch => {
  const uid = await auth().currentUser.uid;
  //console.log('uid', uid);

  const userRes = await firestore()
    .collection('users')
    .doc(uid)
    .get();

  let userPosts = [
    /*{data: 'this is dummy'}*/
  ];
  let postIds = [];

  const {isDisabled, userDisabilities} = userRes.data();
  //console.log('userDisabilities', userDisabilities);
  if (isDisabled && userDisabilities && userDisabilities.length > 0) {
    userDisabilities.forEach(async userDisability => {
      //console.log('testing for disability ', userDisability);
      const {id} = userDisability;
      const postRes = await firestore()
        .collection('posts')
        .where('disabilityIds', 'array-contains', id)
        .orderBy('createdAt', 'DESC')
        .limit(10)
        .get();

      console.log('postRes', postRes);

      if (!postRes.empty) {
        const result = postRes._docs.map(async doc => {
          let postData = doc.data();

          const {likeUserIds} = postData;
          const userDetails = await getUserDetailsById(postData.userId);
          postData.profilePic = userDetails.data.profile;
          postData.userName = userDetails.data.fullname;
          postData.isLiked = false;
          if (likeUserIds && likeUserIds.includes(uid)) {
            postData.isLiked = true;
          }
          return postData;
        });
        Promise.all(result).then(completed => {
          completed.forEach(item => {
            if (!postIds.includes(item.id)) {
              userPosts.push(item);
            }
          });

          dispatch({type: USER_POSTS, payload: userPosts});
          navigation.navigate('Home');
        });
      } else {
        const postResult = await firestore()
          .collection('posts')
          .orderBy('createdAt', 'DESC')
          .limit(10)
          .get();

        if (!postResult.empty) {
          const result = postResult._docs.map(async doc => {
            let postData = doc.data();
            console.log('postData', postData);

            const {likeUserIds} = postData;
            const userDetails = await getUserDetailsById(postData.userId);
            postData.profilePic = userDetails.data.profile;
            postData.userName = userDetails.data.fullname;
            postData.isLiked = false;
            if (likeUserIds.includes(uid)) {
              postData.isLiked = true;
            }
            return postData;
          });
          Promise.all(result).then(completed => {
            completed.forEach(item => {
              if (!postIds.includes(item.id)) {
                userPosts.push(item);
              }
            });

            dispatch({type: USER_POSTS, payload: userPosts});
            navigation.navigate('Home');
          });
        } else {
          navigation.navigate('Home');
        }
      }
      //
    });
  } else {
    const postRes = await firestore()
      .collection('posts')
      .orderBy('createdAt', 'DESC')
      .limit(10)
      .get();

    //console.log('postRes', postRes);
    if (!postRes.empty) {
      const result = postRes._docs.map(async doc => {
        let postData = doc.data();
        //console.log('postData', postData);

        const {likeUserIds} = postData;
        const userDetails = await getUserDetailsById(postData.userId);
        postData.profilePic = userDetails.data.profile;
        postData.userName = userDetails.data.fullname;
        postData.isLiked = false;
        if (likeUserIds.includes(uid)) {
          postData.isLiked = true;
        }
        return postData;
      });
      Promise.all(result).then(completed => {
        completed.forEach(item => {
          if (!postIds.includes(item.id)) {
            userPosts.push(item);
          }
        });

        dispatch({type: USER_POSTS, payload: userPosts});
        navigation.navigate('Home');
      });
    } else {
      navigation.navigate('Home');
    }
  }
};
