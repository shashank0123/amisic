import {
  LOGIN_SUCCESS,
  LOGOUT,
  CREATE_USER,
  LOGIN_ERROR,
  CLEARE_ALL,
  LOADING,
  GET_USER,
  REGISTER_FAIL,
  REGISTER_SUCCESS,
  LOADING_FALSE,
} from '../actions/types';

const initialState = {
  user: null,
  loading: false,
  error: '',
  message: '',
  success: false,
};

const AuthReducer = (state = initialState, action) => {
  switch (action.type) {
    case LOADING: {
      return {
        ...state,
        loading: true,
      };
    }
    case CREATE_USER: {
      return {
        ...state,
        user: action.payload,
      };
    }

    case REGISTER_SUCCESS: {
      return {
        ...state,
        user: action.payload,
        loading: false,
      };
    }
    case LOADING_FALSE: {
      return {
        ...state,
        loading: false,
      };
    }

    case REGISTER_FAIL: {
      return {
        user: null,
        loading: false,
      };
    }
    case LOGIN_ERROR: {
      return {
        ...state,
        user: null,
        loading: false,
      };
    }
    case LOGIN_SUCCESS: {
      return {
        ...state,
        user: action.payload,
        loading: false,
      };
    }
    case LOGOUT: {
      return {
        ...state,
        user: null,
        loading: false,
        success: false,
        message: '',
      };
    }
    case CLEARE_ALL: {
      return {
        ...state,
        loading: false,
      };
    }
    case LOADING: {
      return {
        ...state,
        loading: true,
      };
    }
    case GET_USER: {
      return {
        ...state,
        user: action.payload.user,
      };
    }
    default:
      return state;
  }
};

export default AuthReducer;
