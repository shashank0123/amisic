import {USER_POSTS} from '../actions/types';
const initialState = {
  posts: [],
};

const PostReducer = (state = initialState, action) => {
  //console.log('action', action);
  switch (action.type) {
    case USER_POSTS: {
      //console.log('comes in.........payload', action.payload);
      return {
        ...state,
        posts: action.payload,
      };
    }
    default:
      return state;
  }
};

export default PostReducer;
