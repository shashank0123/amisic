import {combineReducers} from 'redux';
import AuthReducer from './AuthReducer';
import GroupReducer from './GroupReducer';
import FriendsReducer from './FriendsReducer';
import PostReducer from './PostReducer';
// import MessageReducer from './';

const rootReducer = combineReducers({
  auth: AuthReducer,
  group: GroupReducer,
  friends: FriendsReducer,
  posts: PostReducer,
  // home: homeReducer,
  //message: UserReducer,
});

export default rootReducer;
