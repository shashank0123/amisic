import {
  LOADING,
  GET_CURRENT_USER_GROUPS,
  CREATE_CIRCLE_SUCCESS,
  CREATE_CIRCLE_FAIL,
  GET_CIRCLE_UNDER_RANGE,
  CLEARE_ALL,
  GET_CURRENT_USERS_CREATED_GROUPS,
  GET_CIRCLE_SUGGESTIONS,
  GET_GROUP_POSTS,
} from '../actions/types';

const initialState = {
  groups: [],
  createdGroups: [],
  loading: false,
  error: '',
  message: '',
  success: false,
  underRangeCircle: [],
};

const GroupReducer = (state = initialState, action) => {
  switch (action.type) {
    case LOADING: {
      return {
        ...state,
        loading: true,
      };
    }

    case GET_CURRENT_USER_GROUPS: {
      return {
        ...state,
        groups: action.payload,
        loading: false,
      };
    }

    // case GET_GROUP_POSTS: {
    //   return {
    //     ...state,
    //     groupPosts: action.payload,
    //     loading: false,
    //   };
    // }

    case GET_CURRENT_USERS_CREATED_GROUPS: {
      return {
        ...state,
        createdGroups: action.payload,
        loading: false,
      };
    }

    case GET_CIRCLE_SUGGESTIONS: {
      return {
        ...state,
        suggestions: action.payload,
        loading: false,
      };
    }

    case GET_CIRCLE_UNDER_RANGE: {
      return {
        ...state,
        underRangeCircle: action.payload,
        loading: false,
      };
    }

    // case GET_CURRENT_USER_GROUPS: {
    //   return {
    //     ...state,
    //     groups: action.payload,
    //     loading: false,
    //   };
    // }

    case CREATE_CIRCLE_SUCCESS: {
      return {
        ...state,
        loading: false,
      };
    }

    case CLEARE_ALL: {
      return {
        groups: [],
        loading: false,
        error: '',
        message: '',
        success: false,
        underRangeCircle: [],
      };
    }

    default:
      return state;
  }
};

export default GroupReducer;
