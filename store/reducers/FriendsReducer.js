import {
  LOADING,
  GET_FRIENDS_SUCCESS,
  GET_FRIENDS_FAIL,
  CLEARE_ALL,
} from '../actions/types';

const initialState = {
  friends: [],
  loading: false,
  error: '',
  message: '',
  success: false,
};

const FriendsReducer = (state = initialState, action) => {
  switch (action.type) {
    case LOADING: {
      return {
        ...state,
        loading: true,
      };
    }

    case GET_FRIENDS_SUCCESS: {
      return {
        ...state,
        friends: action.payload,
        loading: false,
      };
    }

    case GET_FRIENDS_FAIL: {
      return {
        ...state,

        loading: false,
      };
    }

    case CLEARE_ALL: {
      return {
        friends: [],
        loading: false,
        error: '',
        message: '',
        success: false,
      };
    }

    default:
      return state;
  }
};

export default FriendsReducer;
