import firestore from '@react-native-firebase/firestore';

export const getUserDetailsById = async userId => {
  try {
    const userRes = await firestore()
      .collection('users')
      .doc(userId)
      .get();
    if (userRes && userRes.data()) {
      return {status: 'success', data: userRes.data()};
    }
    return {status: 'error'};
  } catch (err) {
    console.log('err', err);
    return {status: 'error'};
  }
};

export const sendTextMessage = async messageObject => {
  try {
    const {conversationId, from, to, message} = messageObject;
    let batch = firestore().batch();

    if (conversationId) {
      let conversationRef = firestore()
        .collection('conversations')
        .doc(conversationId);

      let messageRef = firestore()
        .collection('messages')
        .doc();

      batch.update(conversationRef, {
        recentMessage: message,
      });

      batch.set(messageRef, {
        id: messageRef.id,
        conversationId,
        from,
        to,
        message,
        createdAt: firestore.FieldValue.serverTimestamp(),
        memberIds: [from, to],
      });

      batch.commit();
    } else {
      console.log('comes to else');
      const {senderName, senderProfile} = messageObject;
      let conversationRef = firestore()
        .collection('conversations')
        .doc();

      console.log('conId', conversationRef.id);

      let messageRef = firestore()
        .collection('messages')
        .doc();

      batch.set(conversationRef, {
        id: conversationRef.id,
        memberIds: [from, to],
        senderId: from,
        senderName,
        senderProfile,
        recentMessage: message,
      });
      batch.set(messageRef, {
        id: messageRef.id,
        conversationId: conversationRef.id,
        from,
        to,
        message,
        createdAt: firestore.FieldValue.serverTimestamp(),
        memberIds: [from, to],
      });

      batch.commit();
      console.log('sent empty message');
    }

    return {status: 'success'};
  } catch (err) {
    console.log('err', err);
    return {status: 'error'};
  }
};
