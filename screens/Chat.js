import React, { useEffect, useRef, useState } from 'react';
import { View, Text, Alert, StyleSheet, Keyboard, Platform, SafeAreaView } from 'react-native';
import { useRoute } from '@react-navigation/native';
import Header from '../components/header';
import Message from '../components/message';
import ChatInput from '../components/input';
import Feather from 'react-native-vector-icons/Feather';
import {
  ScrollView,
  TouchableWithoutFeedback,
  TouchableOpacity,
  FlatList,
} from 'react-native-gesture-handler';
import colors from '../constants/colors';
import firestore from '@react-native-firebase/firestore';
import auth from '@react-native-firebase/auth';
import { sendTextMessage } from '../utils';

class Chat extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      messages: [],
      messageText: '',
      limit: 10,
      firstTime: true,
    };
  }

  forceRerender = () => {
    this.onChangeMessageText('Enter a message here...');
    setTimeout(() => this.onChangeMessageText(''), 100);
  };
  componentDidMount = () => {
    this.messageListener();
  };

  messageListener = () => {
    const { params } = this.props.route;
    const { conversationId } = params;
    let { messages, limit } = this.state;

    limit += 20;

    firestore()
      .collection('messages')
      .where('conversationId', '==', conversationId)
      .orderBy('createdAt', 'DESC')
      .limit(limit)
      .onSnapshot(snapshot => {
        if (!snapshot.empty) {
          this.forceRerender();
          this.setState({ limit });
          snapshot._docs.forEach(doc => {
            //console.log('doc', doc);
            const { id, from, to, message, createdAt } = doc.data();
            console.log(createdAt)

            if (messages.some(messageItem => messageItem.id === id)) {
              //existing message
              let index = messages.indexOf(
                messages.filter(messageItem => messageItem.id === id)[0],
              );
              messages[index] = { id, from, to, message };
            } else {
              if (!createdAt) {
                messages.unshift({ id, from, to, message });
                this.setState({ firstTime: true });
              } else {
                const arrivingTime = new Date(createdAt.seconds * 1000);
                let messageTime = arrivingTime.getMonth() + '/' + arrivingTime.getDate() + '/' + arrivingTime.getFullYear() + ' ' + arrivingTime.getHours() + ':' + arrivingTime.getMinutes()
                if (
                  arrivingTime.getMinutes() === new Date().getMinutes() &&
                  arrivingTime.getHours() === new Date().getHours() &&
                  arrivingTime.getDate() === new Date().getDate() &&
                  arrivingTime.getMonth() === new Date().getMonth() &&
                  arrivingTime.getFullYear() === new Date().getFullYear()
                ) {
                  messages.unshift({ id, from, to, message, messageTime });
                  this.setState({ firstTime: true });
                } else {
                  messages.push({ id, from, to, message, messageTime });
                }
              }
            }
          });
          this.setState({ messages });
        }
      });
  };

  onChangeMessageText = messageText => {
    this.setState({ messageText });
  };

  sendMessage = async () => {
    const { messageText } = this.state;
    Keyboard.dismiss();
    if (!messageText || messageText.length < 1) {
      return;
    }
    const { params } = this.props.route;
    const { conversationId, senderId } = params;
    const loggedInId = auth().currentUser.uid;
    const messageObject = {
      conversationId,
      from: loggedInId,
      to: senderId,
      message: messageText,
    };
    //console.log('messageObject', messageObject);
    const res = await sendTextMessage(messageObject);
    //console.log('res', res);
    this.setState({ messageText: '' });
    setTimeout(() => this.onChangeMessageText(' '), 600);
    setTimeout(() => this.onChangeMessageText(''), 800);
    //Alert.alert(messageText);
  };

  render() {
    const { params } = this.props.route;
    const { title, conversationId } = params;
    let { messages, messageText, firstTime } = this.state;
    // messages.reverse();
    const loggedInId = auth().currentUser.uid;
    return (
      <SafeAreaView style={{flex:1,justifyContent:'flex-start' }}>
      <View style={styles.container}>
        <Header navigation={this.props.navigation} icon={true} title={title} />
        <ScrollView
          ref={ref => {
            this.scrollView = ref;
          }}
          onScroll={e => {
            let paddingToBottom = 10;
            paddingToBottom += e.nativeEvent.layoutMeasurement.height;
            if (e.nativeEvent.contentOffset.y <= 10) {
              //console.log('comes to end');
              this.setState({ firstTime: false });
              firestore()
                .collection('messages')
                .where('conversationId', '==', conversationId)
                .get()
                .then(res => {
                  //console.log('.......', res._docs.length);
                  if (!res.empty && res._docs.length > messages.length) {
                    this.messageListener();
                  }
                });
            }
          }}
          onContentSizeChange={() => {
            if (firstTime) {
              this.scrollView.scrollToEnd();
            }
          }}
          style={{ paddingTop: 10 }}>
          <View
            style={{
              paddingHorizontal: 10,
              flex: 1,
              justifyContent: 'flex-end',
            }}>
            <TouchableWithoutFeedback>
              <FlatList
                inverted
                data={messages}
                renderItem={message => {
                  console.log('message', message);
                  if (message.item.message) {
                    return (
                      <Message
                        messageTime={message.item.messageTime}
                        me={message.item.from === loggedInId}
                        message={message.item.message}
                      />
                    );
                  } else {
                    return null;
                  }
                }} />
            </TouchableWithoutFeedback>
          </View>
        </ScrollView>

        <View style={styles.chatInput}>
          <ChatInput
            myStyles={{
              minHeight: 60,
              flex: 1,
              color: '#262626',
              fontSize: 18,
              borderRadius: 0,
              backgroundColor: 'white',
            }}
            placeholder="Enter a message here..."
            value={messageText}
            onChangeText={this.onChangeMessageText}
            multiline={true}
            placeholderTextColor="black"
          />
          <View style={{ flexDirection: 'row' }}>
            <TouchableOpacity style={styles.icon} onPress={this.sendMessage}>
              <Feather name="send" size={25} color={colors.theme} />
            </TouchableOpacity>
          </View>
        </View>
      </View>
      </SafeAreaView >
    );
  }
}
export default Chat;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },

  chatBox: {
    flex: 1,
    backgroundColor: '#fff',
  },
  chatInput: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 10,
    borderTopWidth: 0.5,
    borderTopColor: 'rgba(0,0,0,0.2)',
    marginTop: 5,
  },
  icon: {
    marginRight: 10,
  },
});
