import React, { useState } from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  Dimensions,
  Modal,
  StatusBar,
  SafeAreaView
} from 'react-native';
import Header from '../components/header';
import { TouchableOpacity, ScrollView } from 'react-native-gesture-handler';
import Button from '../components/button';
import ProfileEdit from '../components/profileEdit';
import { useSelector, useDispatch } from 'react-redux';
import { logout } from '../store/actions/Auth';
import { useNavigation } from '@react-navigation/native';
import colors from '../constants/colors';
const { width, height } = Dimensions.get('window');

const Profile = props => {
  const [state, setState] = useState({
    edit: false,
  });

  const auth = useSelector(state => state.auth);
  const posts = useSelector(state => state.posts);
  const dispatch = useDispatch();
  const navigation = useNavigation();

  const getDisabilities = () => {
    let disabilityText = '';
    auth.user.userDisabilities.forEach(disability => {
      let titleCaseDisablity =
        disability.name.substring(0, 1).toUpperCase() +
        disability.name.substring(1);
      disabilityText += titleCaseDisablity + ', ';
    });
    if (disabilityText) {
      return disabilityText.substring(0, disabilityText.length - 2);
    }
    return '';
  };

  const closeModal = () => {
    setState({ ...state, edit: false });
  };

  const openModal = () => {
    setState({ ...state, edit: !state.edit });
  };

  const saveProfile = () => {
    setState({ ...state, edit: false });
  };
  return (
    <>
      <SafeAreaView style={{flex:1,justifyContent:'flex-start'}}>
      <StatusBar backgroundColor="white" barStyle="dark-content" />
      <Modal animated={true} onRequestClose={closeModal} visible={state.edit}>
        <ProfileEdit closeProfileModal={closeModal} saveProfile={saveProfile} />
      </Modal>
      <ScrollView>
        <View style={styles.container}>
          <Header title="Profile" icon={false} />
          <View style={styles.imageContainer}>
            <Image
              source={{ uri: auth.user ? auth.user.profile : 'https://cdn4.iconfinder.com/data/icons/e-commerce-181/512/477_profile__avatar__man_-512.png' }}
              style={{ width: null, height: null, flex: 1 }}
            />
          </View>
          <View style={styles.detailContainer}>
            <View style={styles.infoList}>
              <Text style={styles.title}>Username:</Text>
              <Text style={styles.text}>
                {auth.user ? auth.user.fullname : null}
              </Text>
            </View>
            <View style={styles.infoList}>
              <Text style={styles.title}>Email:</Text>
              <Text style={styles.text}>
                {auth.user ? auth.user.email : null}
              </Text>
            </View>
            {auth.user && auth.user.isDisabled && (
              <View style={styles.infoList}>
                <Text style={styles.title}>{'Disabilities'}:</Text>
                <Text style={styles.text}>{getDisabilities()}</Text>
                {/* {auth.user.userDisabilities.map(disability => {
                  return (
                    <Text style={styles.text}>{disability.name + ', '}</Text>
                  );
                })} */}
                {/* <Text style={styles.text}>
                  {auth.user ? auth.user.userDisabilities[0].name : null}
                </Text> */}
              </View>
            )}
          </View>
          <View>
            <Button
              myStyles={{ backgroundColor: colors.theme }}
              onPress={openModal}
              title="Edit Profile"
            />
          </View>

          <View>
            <Button
              myStyles={{ marginTop: 20, backgroundColor: colors.theme }}
              onPress={() => dispatch(logout(navigation))}
              title="Logout"
            />
          </View>
        </View>
      </ScrollView>
      </SafeAreaView>
      
    </>
  );
};
export default Profile;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },

  imageContainer: {
    width: 200,
    height: 200,
    borderRadius: 200 / 2,
    backgroundColor: '#f4f4f4',
    alignSelf: 'center',
    marginVertical: 10,
    overflow: 'hidden',
  },

  detailContainer: {
    padding: 10,
  },

  infoList: {
    flexDirection: 'row',
    borderBottomColor: 'rgba(0,0,0,0.1)',
    borderBottomWidth: 0.5,
    padding: 10,
  },
  title: {
    fontWeight: 'bold',
    fontSize: 15,
    marginRight: 10,
  },

  text: {
    fontSize: 15,
  },
});
