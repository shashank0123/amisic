import React from 'react';
import {View, Text, FlatList, StyleSheet,SafeAreaView} from 'react-native';
import FriendRequestCard from '../../components/friendRequestCard';
import firestore from '@react-native-firebase/firestore';
import auth from '@react-native-firebase/auth';
export default class FriendRequest extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      requests: [],
    };
  }

  componentDidMount = async () => {
    const uid = auth().currentUser.uid;
    //let {requests} = this.state;
    firestore()
      .collection('friendRequests')
      .where('toId', '==', uid)
      .onSnapshot(snapshot => {
        if (!snapshot.empty) {
          let requests = [];
          snapshot._docs.forEach(doc => {
            const { fromId, fromName, fromProfile, id } = doc.data();
            requests.push({
              id,
              fromId,
              fromName,
              fromProfile,
            });
          });
          this.setState({ requests });
        } else {
          this.setState({ requests: [] });
        }
      });
  };
  render() {
    const { requests } = this.state;
    return (
      <SafeAreaView style={{flex:1,justifyContent:'flex-start', paddingTop: 20  }}>
        <View style={styles.container}>
        {requests.length ? (
          <FlatList
            numColumns="2"
            data={requests}
            keyExtractor={data => data.key}
            renderItem={request => (
              <FriendRequestCard
                id={request.item.id}
                fromId={request.item.fromId}
                name={request.item.fromName}
                profile={request.item.fromProfile}
              />
            )}
          />
        ) : (
            <Text style={styles.headerTextStyle}>No Friend Requests</Text>
          )}
      </View>
      </SafeAreaView>
      
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  headerTextStyle: {
    fontWeight: 'bold',
    textAlign: 'center',
    fontSize: 18,
    padding: 10,
    color: 'black',
  },
});
