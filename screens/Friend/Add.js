import React, {useEffect} from 'react';
import {View, Text, TouchableOpacity, StyleSheet,SafeAreaView, Modal} from 'react-native';
import AddFriendCard from '../../components/AddFriendCard';
import { useDispatch, useSelector } from 'react-redux';
import { getAllFriendsSuggestion } from '../../store/actions/Friends';
import { FlatList } from 'react-native-gesture-handler';
import firestore from '@react-native-firebase/firestore';
import auth from '@react-native-firebase/auth';
import { getUserDetailsById } from '../../utils';
import Ionicons from 'react-native-vector-icons/Ionicons';
import SearchFriend from './SearchFriend';
import colors from '../../constants/colors';

export default class AddFriend extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      friends: [],
      searchFriend: false,
    };
  }

  componentDidMount = async () => {
    this.setState({ loading: true });
    const { friendIds, requestedIds, incomingRequestIds } = this.props;
    const uid = auth().currentUser.uid;

    let suggestionIds = [];
    let friends = [];

    const myRes = await firestore()
      .collection('users')
      .doc(uid)
      .get();

    const myDisabilities = myRes.data().userDisabilities;

    const friendres = await firestore()
      .collection('circles')
      .where('userIds', 'array-contains', uid)
      .limit(10)
      .get();

    if (!friendres.empty) {
      friendres._docs.forEach(doc => {
        const { userIds } = doc.data();
        userIds.forEach(userId => {
          if (!suggestionIds || !suggestionIds.includes(userId)) {
            suggestionIds.push(userId);
          }
        });
      });
      if (suggestionIds && suggestionIds.length) {
        suggestionIds = suggestionIds.filter(
          suggestionId => suggestionId !== uid,
        );
        if (friendIds && friendIds.length) {
          suggestionIds = suggestionIds.filter(
            suggestionId => !friendIds.includes(suggestionId),
          );
        }
        if (requestedIds && requestedIds.length) {
          suggestionIds = suggestionIds.filter(
            suggestionId => !requestedIds.includes(suggestionId),
          );
        }
        if (incomingRequestIds && incomingRequestIds.length) {
          suggestionIds = suggestionIds.filter(
            suggestionId => !incomingRequestIds.includes(suggestionId),
          );
        }
      }

      //console.log('suggestionIds after filter', suggestionIds);

      //PAGINATION
      if (suggestionIds.length > 20) {
        suggestionIds.slice(0, 20);
      }

      let result = suggestionIds.map(async suggestionId => {
        const userRes = await getUserDetailsById(suggestionId);
        const { fullname, profile, email, userDisabilities } = userRes.data;
        return { fullname, profile, email, userDisabilities, id: suggestionId };
      });

      Promise.all(result).then(completed => {
        completed.forEach(item => {
          const { fullname, profile, email, id, userDisabilities } = item;
          friends.push({
            fullname,
            profile,
            email,
            userDisabilities,
            id,
          });
        });

        //Now search for friends with same disabilities

        const newResult = myDisabilities.map(async myDisability => {
          const frRes = await firestore()
            .collection('users')
            .where('userDisabilities', 'array-contains', myDisability)
            .limit(5)
            .get();
          return frRes._docs;
        });

        Promise.all(newResult).then(completedItems => {
          completedItems.forEach(item => {
            item.forEach(friendItem => {
              const {
                userId,
                fullname,
                profile,
                email,
                userDisabilities,
              } = friendItem.data();
              //console.log('friendItem', friendItem);
              if (
                userId !== uid &&
                (!friendIds || !friendIds.includes(userId)) &&
                (!requestedIds || !requestedIds.includes(userId)) &&
                (!incomingRequestIds || !incomingRequestIds.includes(userId)) &&
                (!friends || !friends.some(person => person.id === userId))
              ) {
                friends.push({
                  fullname,
                  profile,
                  email,
                  userDisabilities,
                  id: userId,
                });
              }
            });
            this.setState({ friends, loading: false });
          });
        });
      });
    } else {
      const result = myDisabilities.map(async myDisability => {
        const frRes = await firestore()
          .collection('users')
          .where('userDisabilities', 'array-contains', myDisability)
          .get();
        return frRes._docs;
      });

      Promise.all(result).then(completed => {
        completed.forEach(item => {
          item.forEach(friendItem => {
            const {
              userId,
              fullname,
              profile,
              email,
              userDisabilities,
            } = friendItem.data();
            //console.log('friendItem', friendItem);
            if (
              userId !== uid &&
              (!friendIds || !friendIds.includes(userId)) &&
              (!requestedIds || !requestedIds.includes(userId)) &&
              (!incomingRequestIds || !incomingRequestIds.includes(userId)) &&
              (!friends || !friends.some(person => person.id === userId))
            ) {
              friends.push({
                fullname,
                profile,
                email,
                userDisabilities,
                id: userId,
              });
            }
          });
          this.setState({ friends, loading: false });
        });
      });
    }
  };

  openModal = () => {
    this.setState({ searchFriend: true });
  };
  closeModal = () => {
    this.setState({ searchFriend: false });
    //dispatch(getCurrentUsersCreatedGroups());
  };

  render() {
    const { friends, loading, searchFriend } = this.state;
    return (
      <SafeAreaView style={{flex:1,justifyContent:'flex-start'}}>
        <View style={styles.container}>
        <Modal
          animated={true}
          visible={searchFriend}
          onRequestClose={this.closeModal}>
          <SearchFriend friends={friends} closeModal={this.closeModal} />
        </Modal>
        {friends.length ? (
          <FlatList
            numColumns="2"
            data={friends}
            keyExtractor={data => data.key}
            renderItem={data => <AddFriendCard data={data.item} />}
          />
        ) : (
            <Text style={styles.headerTextStyle}>
              {loading ? 'Loading... Please Wait!' : 'There is no friend'}
            </Text>
          )}
        <TouchableOpacity onPress={this.openModal} style={styles.position} accessible={true} accessibilityLabel={'Search'}>
          <View style={styles.addBtn}>
            <Ionicons name="md-search" size={24} color="white" />
          </View>
        </TouchableOpacity>
      </View>
      </SafeAreaView>
      
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  headerTextStyle: {
    fontWeight: 'bold',
    textAlign: 'center',
    fontSize: 18,
    padding: 10,
    color: 'black',
  },
  position: {
    position: 'absolute',
    bottom: 20,
    right: 20,
  },

  addBtn: {
    width: 50,
    height: 50,
    borderRadius: 25,

    backgroundColor: colors.theme,
    justifyContent: 'center',
    alignItems: 'center',
  },

  addBtnText: {
    color: 'white',
    fontSize: 15,
  },
});
