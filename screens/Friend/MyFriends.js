import React, { useEffect } from 'react';
import { View, Text, StyleSheet,SafeAreaView } from 'react-native';
import AddFriendCard from '../../components/AddFriendCard';
import { useDispatch, useSelector } from 'react-redux';
import { getAllFriendsSuggestion } from '../../store/actions/Friends';
import { FlatList } from 'react-native-gesture-handler';
import firestore from '@react-native-firebase/firestore';
import auth from '@react-native-firebase/auth';
import { getUserDetailsById } from '../../utils';

export default class MyFriends extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      friends: [],
    };
  }

  componentDidMount = async () => {
    const uid = auth().currentUser.uid;
    let friends = [];
    // alert(uid);
    const friendres = await firestore()
      .collection('users')
      .doc(uid)
      .get();
    console.log(friendres);

    if (friendres.exists) {
      console.log('exists');
      const { friendIds } = friendres.data();
      console.log('friendIds', friendIds);
      if (friendIds) {
        let result = friendIds.map(async friendId => {
          const userRes = await firestore()
            .collection('users')
            .doc(friendId)
            .get();
          const { userId, fullname, profile } = userRes.data();

          return { userId, fullname, profile };
        });

        Promise.all(result).then(completed => {
          completed.forEach(item => {
            const { userId, fullname, profile } = item;
            friends.push({ userId, fullname, profile });
          });
          this.setState({ friends });
        });
      }
    }
  };

  render() {
    const { friends } = this.state;
    return (
      <SafeAreaView style={{flex:1,justifyContent:'flex-start'}}>
        <View style={styles.container}>
        {friends.length ? (
          <FlatList
            numColumns="2"
            data={friends}
            keyExtractor={data => data.key}
            renderItem={data => (
              <AddFriendCard data={{ ...data.item, noButton: true }} />
            )}
          />
        ) : (
            <Text style={styles.headerTextStyle}>There is no friend</Text>
          )}
      </View>
      </SafeAreaView>
      
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  headerTextStyle: {
    fontWeight: 'bold',
    textAlign: 'center',
    fontSize: 18,
    padding: 10,
    color: 'black',
  },
});
