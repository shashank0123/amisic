import React from 'react';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import {SafeAreaView} from 'react-native';
import { useSelector, useDispatch } from 'react-redux';

import FriendRequest from './Request';
import AddFriend from './Add';
import MyFriends from './MyFriends';

const Tab = createMaterialTopTabNavigator();

const Friends = props => {
  const auth = useSelector(state => state.auth);
  return (
    <SafeAreaView style={{flex:1,justifyContent:'flex-start', paddingTop: 20  }}>
    <Tab.Navigator>
      <Tab.Screen name="Friend Request"
        component={() => (<FriendRequest />)}
      />
      <Tab.Screen name="My Friends"
        component={() => (<MyFriends />)}
      />

      <Tab.Screen
        name="Add Friend"
        component={() => (
          <AddFriend
            friendIds={auth.user ? auth.user.friendIds : []}
            requestedIds={auth.user ? auth.user.requestedIds : []}
            incomingRequestIds={auth.user ? auth.user.incomingRequestIds : []}
          />
        )}
      />
    </Tab.Navigator>
    </SafeAreaView>
  );
};
export default Friends;
