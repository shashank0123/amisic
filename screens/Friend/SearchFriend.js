import React from 'react';
import { Text, View, StyleSheet, TouchableOpacity, SafeAreaView } from 'react-native';
import { SearchBar } from '../../components/SearchBar';
import colors from '../../constants/colors';
import { FlatList } from 'react-native-gesture-handler';
import AddFriendCard from '../../components/AddFriendCard';


import algoliasearch from "algoliasearch/reactnative";
// import { InstantSearch, SearchBox, InfiniteHits,  } from 'react-instantsearch-native';

import { InstantSearch } from "react-instantsearch-native";
import SearchBox from "../../components/algoliaSearchComponent/SearchBox";
import InfiniteHits from "../../components/algoliaSearchComponent/InfiniteHits";

// const searchClient = algoliasearch(env.algolia.appid, env.algolia.apikey)
//const searchClient = algoliasearch(
//  "KYAO8ZF1KL",
//  "f8b258cfa0889ce09e7351822d5b9b76"
//);

const algoliaClient = algoliasearch(
  "KYAO8ZF1KL",
  "f8b258cfa0889ce09e7351822d5b9b76"
);

const searchClient = {
  search(requests) {
    if (requests.every(({ params }) => !params.query)) {
      // console.log(params.query);

      return Promise.resolve({
        results: requests.map(() => ({
          hits: [],
          nbHits: 0,
          nbPages: 0,
          processingTimeMS: 0,
        })),
      });
    }

    return algoliaClient.search(requests);
  },
};

export default class SearchFriend extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      queryEnter: false,
      suggestionVisible: false,
      suggestionFriends: [],
    };
  }

  onSearch = searchText => {
    if (!searchText || searchText.trim().length === 0) {
      this.setState({ suggestionFriends, suggestionVisible: false });
      return;
    }
    const { friends } = this.props;
    let suggestionFriends = [];
    friends.forEach(friend => {
      //console.log('friend', friend);
      let frName = friend.fullname.trim().toLowerCase();
      let searching = searchText.trim().toLowerCase();
      if (frName.indexOf(searching) !== -1) {
        //console.log('comes in');
        const { fullname, profile, email, userDisabilities, id } = friend;
        suggestionFriends.push({
          fullname,
          profile,
          email,
          userDisabilities,
          id,
        });
      }
    });
    //console.log(suggestionFriends);
    this.setState({ suggestionFriends, suggestionVisible: true });
  };
  renderSuggestions = () => {
    const { suggestionFriends } = this.state;
    const { closeModal } = this.props;
    console.log(closeModal);
    if (!suggestionFriends || suggestionFriends.length < 1) {
      return (
        <Text
          style={{
            textAlign: 'center',
            marginLeft: 5,
            marginBottom: 5,
            color: colors.theme,
            fontWeight: 'bold',
          }}>
          No suggestions found!
        </Text>
      );
    }
    return (
      <View style={styles.suggestionsBox}>
        <Text
          style={{
            textAlign: 'center',
            marginLeft: 5,
            marginBottom: 5,
            color: colors.theme,
            fontWeight: 'bold',
          }}>
          Search Result
        </Text>
        <FlatList
          numColumns="2"
          data={suggestionFriends}
          keyExtractor={data => data.key}
          renderItem={data => (
            <AddFriendCard data={{ ...data.item, closeModal: closeModal }} />
          )}
        />
      </View>
    );
  };
  render() {
    const { suggestionVisible } = this.state;
    const { closeModal } = this.props;
    const root = {
      Root: View,
      props: {
        style: {
          flex: 1,
        },
      },
    };

    return (
      <>
      <SafeAreaView style={{flex:1,justifyContent:'flex-start' }}>
        <TouchableOpacity onPress={closeModal}>
          <View style={{ alignItems: 'flex-end', paddingRight: 10, paddingTop: 10 }}>
            <Text style={{ fontWeight: 'bold', fontSize: 18, color: colors.theme }}>Close</Text>
          </View>
        </TouchableOpacity>
        <InstantSearch
          searchClient={searchClient}
          indexName="users"
          root={root}
          onSearchStateChange={searchState =>
            console.log('=====> ', searchState)
          }
        >
          <View >
            <View>
              <SearchBox
                accessible={true} accessibilityLabel={'Search'}
              />
              <InfiniteHits type="users" closeModal={closeModal} />
            </View>
          </View>
        </InstantSearch>
        </SafeAreaView>
      </>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },

  imageContainer: {
    width: 200,
    height: 200,
    borderRadius: 100,
    backgroundColor: '#f8f8f8',
    alignSelf: 'center',
    overflow: 'hidden',
  },

  choosePicture: {
    alignItems: 'center',
    marginVertical: 20,
  },
  choosePictureText: {
    fontSize: 16,
    fontWeight: 'bold',
    color: 'tomato',
  },

  label: {
    fontSize: 15,
    fontWeight: 'bold',
    marginLeft: 5,
    marginBottom: 5,
  },

  formGroup: {
    marginHorizontal: 20,
    marginBottom: 10,
  },
  buttonText: {
    color: colors.theme,
    fontWeight: 'bold',
  },
  suggestionsBox: {
    flex: 1,
  },
});
