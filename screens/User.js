import React from 'react';
import { View, Text, StyleSheet, Image, SafeAreaView } from 'react-native';
import Header from '../components/header';
import { useRoute, useNavigation } from '@react-navigation/native';
import Button from '../components/button';
import colors from '../constants/colors';
import firestore from '@react-native-firebase/firestore';
import auth from '@react-native-firebase/auth';
import { useSelector } from 'react-redux';
import { getUserDetailsById } from '../utils';
import { useDispatch } from 'react-redux';
import { getCurrentUser } from '../store/actions/Auth';

const FriendProfile = props => {
  const route = useRoute();
  const navigation = useNavigation();
  const dispatch = useDispatch();
  let { profile } = route.params;
  if (!profile) {
    profile = {};
  }

  const auth = useSelector(state => state.auth);
  // console.log('auth>>',auth);

  //   console.log('profile', profile);

  const getDisabilities = userDisabilities => {
    let disabilityText = '';
    userDisabilities.forEach(disability => {
      let titleCaseDisablity =
        disability.name.substring(0, 1).toUpperCase() +
        disability.name.substring(1);
      disabilityText += titleCaseDisablity + ', ';
    });
    if (disabilityText) {
      return disabilityText.substring(0, disabilityText.length - 2);
    }
    return '';
  };

  const onAddFrindPress = async () => {
    try {
      const uid = auth.user.userId//().currentUser.uid;
      const myRes = await getUserDetailsById(uid);
      const batch = firestore().batch();
      const userRef = firestore()
        .collection('users')
        .doc(uid);

      batch.update(userRef, {
        requestedIds: firestore.FieldValue.arrayUnion(profile.id),
      });

      const toUserRef = firestore()
        .collection('users')
        .doc(profile.id);

      batch.update(toUserRef, {
        incomingRequestIds: firestore.FieldValue.arrayUnion(uid),
      });

      const friendReqRef = firestore()
        .collection('friendRequests')
        .doc();
      batch.set(friendReqRef, {
        id: friendReqRef.id,
        fromId: uid,
        fromName: myRes.data.fullname,
        fromProfile: myRes.data.profile,
        toId: profile.id,
        requestedAt: firestore.FieldValue.serverTimestamp(),
      });
      batch.commit();
      dispatch(getCurrentUser());
      navigation.navigate('Home');
    } catch (err) {
      console.log('err', err);
      navigation.navigate('Home');
    }
  };

  return (
    <SafeAreaView style={{flex:1,justifyContent:'flex-start', paddingTop: 20  }}>
    <View style={styles.container}>
      <Header navigation={navigation} icon={true} title={profile.fullname} />

      <View style={styles.imageContainer}>
        <Image
          style={{ width: null, height: null, flex: 1 }}
          source={{
            uri:
              profile.profile ||
              'https://i.ya-webdesign.com/images/instagram-profile-photo-png-2.png',
          }}
        />
      </View>
      <View style={{ alignItems: 'center' }}>
        <Text style={styles.name}>{profile.fullname}</Text>
        <Text style={styles.email}>{profile.email}</Text>
        {profile.userDisabilities && (
          <Text>
            {'Disabilities: ' + getDisabilities(profile.userDisabilities)}
          </Text>
        )}
      </View>
      {!profile.noButton && (
        <Button
          onPress={
            auth.user.requestedIds &&
              auth.user.requestedIds.includes(profile.id)
              ? null
              : onAddFrindPress
          }
          title={
            auth.user.requestedIds &&
              auth.user.requestedIds.includes(profile.id)
              ? 'Request Sent'
              : 'Add Friend'
          }
          myStyles={{ backgroundColor: colors.theme, marginVertical: 20 }}
        />
      )}
    </View>
    </SafeAreaView>
  );
};
export default FriendProfile;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },

  imageContainer: {
    marginVertical: 20,
    alignSelf: 'center',
    elevation: 10,
    height: 250,
    width: 250,
    borderRadius: 250 / 2,
    overflow: 'hidden',
    backgroundColor: '#f8f8f8',
  },
  name: {
    fontSize: 35,
    fontWeight: 'bold',
    marginLeft: 10,
    textAlign: 'center',
  },

  email: {
    fontSize: 16,
  },
});
