import React, { useEffect, Component } from 'react';
import { View, SafeAreaView, Text, StyleSheet } from 'react-native';
import Loader from '../components/loader';
import Header from '../components/header';
import FeedCard from '../components/FeedCard';
import { FlatList } from 'react-native-gesture-handler';
import { useSelector, useDispatch } from 'react-redux';
import { getPosts } from '../store/actions/Home';
import { BannerAdSize, BannerAd, TestIds } from '@react-native-firebase/admob';

const Feeds = props => {
  // const dispatch = useDispatch();
  const auth = useSelector(state => state.auth);
  const posts = useSelector(state => state.posts);

  return (
    <SafeAreaView style={{ justifyContent:'flex-start',flex:1 }}>
      <View style={styles.container}>
      {/* change banner id by changing {TestIds.BANNER} to "your id"  */}
      <BannerAd size={BannerAdSize.SMART_BANNER} unitId={TestIds.BANNER} />
      <Header icon={false} title="Feeds" />

      <FlatList
        data={posts.posts}
        keyExtractor={data => data.username}
        renderItem={({ item }) => <FeedCard data={item} />}
      />
      </View>
    </SafeAreaView>
    
  );
};
export default Feeds;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
});
