import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, Modal, SafeAreaView } from 'react-native';
import { TouchableOpacity, FlatList } from 'react-native-gesture-handler';
import CreateCircle from '../../components/createCircle';
import GroupCard from '../../components/GroupCard';
import { useDispatch, useSelector } from 'react-redux';
import { getCurrentUsersCreatedGroups } from '../../store/actions/groups';
import Loader from '../../components/loader';
import colors from '../../constants/colors';

const CreatedGroups = props => {
  const [state, setState] = useState({
    createCircle: false,
  });

  const openModal = () => {
    setState({ ...state, createCircle: true });
  };
  const closeModal = () => {
    setState({ ...state, createCircle: false });
    dispatch(getCurrentUsersCreatedGroups());
  };

  const groups = useSelector(state => state.group.createdGroups);
  console.log('groups', groups);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getCurrentUsersCreatedGroups());
  }, [dispatch]);

  return (
    <>
    <SafeAreaView style={{flex:1,justifyContent:'center'}}>
    <Modal
        animated={true}
        visible={state.createCircle}
        onRequestClose={closeModal}>
        <CreateCircle closeModal={closeModal} />
      </Modal>
      <View style={styles.container}>
        {groups && groups.length ? (
          <FlatList
            numColumns="2"
            data={groups}
            keyExtractor={data => data.key}
            renderItem={data => <GroupCard data={data.item} />}
          />
        ) : (
            <View style={{ marginVertical: 10 }}>
              <Text
                style={{ fontSize: 15, fontWeight: 'bold', alignSelf: 'center' }}>
                No Group yet
            </Text>
            </View>
          )}

        <TouchableOpacity onPress={openModal} containerStyle={styles.position}>
          <View style={styles.addBtn}>
            <Text style={styles.addBtnText}
              accessible={true} accessibilityLabel={'Add Circle'}
            >+</Text>
          </View>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
      
    </>
  );
};
export default CreatedGroups;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },

  position: {
    position: 'absolute',
    bottom: 20,
    right: 20,
  },

  addBtn: {
    width: 50,
    height: 50,
    borderRadius: 25,

    backgroundColor: colors.theme,
    justifyContent: 'center',
    alignItems: 'center',
  },

  addBtnText: {
    color: 'white',
    fontSize: 15,
  },
});
