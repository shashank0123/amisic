import React from 'react';
import {SafeAreaView} from 'react-native';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import MyGroup from './mygroup';
import Groups from './groups';
import CreatedGroups from './CreatedGroups';
import Header from '../../components/header';

const Tab = createMaterialTopTabNavigator();
const Circle = props => {
  return (
    <>
    <SafeAreaView style={{flex:1,justifyContent:'flex-start'}}>
    <Header title="Circle" icon={false} />
      <Tab.Navigator>
        <Tab.Screen name="My Circles" component={CreatedGroups} />
        <Tab.Screen name="Circles" component={MyGroup} />
        <Tab.Screen name="Suggestions" component={Groups} />
      </Tab.Navigator>
    </SafeAreaView>
      
    </>
  );
};
export default Circle;
