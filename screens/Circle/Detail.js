import React, { useState, useEffect } from 'react';
import {
  View,
  Text,
  ScrollView,
  StyleSheet,
  Image,
  Dimensions,
  SafeAreaView,
  Modal,
} from 'react-native';

import { useRoute } from '@react-navigation/native';
import Header from '../../components/header';
import colors from '../../constants/colors';
import { TouchableOpacity, FlatList } from 'react-native-gesture-handler';
import CreateFeeds from '../../components/createFeed';
import FeedCard from '../../components/FeedCard';
import { navigation } from '@react-navigation/native';
import auth from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore';
import { getUserDetailsById } from '../../utils';
const { width, height } = Dimensions.get('window');

export default class Detail extends React.Component {
  constructor(props) {
    super(props);
    const { circle } = this.props.route.params;
    this.state = {
      createFeed: false,
      groupPosts: [],
      circle,
    };
  }

  componentDidMount = () => {
    const { circle } = this.props.route.params;
    this.setState({ circle });
    const uid = auth().currentUser.uid;
    firestore()
      .collection('posts')
      .where('circleId', '==', circle.id)
      .orderBy('createdAt', 'DESC')
      .limit(20)
      .onSnapshot(snapshot => {
        if (!snapshot.empty) {
          let newPosts = [];
          const result = snapshot._docs.map(async doc => {
            let postData = doc.data();
            const { likeUserIds } = postData;
            const userDetails = await getUserDetailsById(postData.userId);
            postData.profilePic = userDetails.data.profile;
            postData.userName = userDetails.data.fullname;
            postData.isLiked = false;
            if (likeUserIds && likeUserIds.includes(uid)) {
              postData.isLiked = true;
            }

            //console.log('data', postData);
            return postData;
          });
          Promise.all(result).then(completed => {
            completed.forEach(item => {
              newPosts.push(item);
            });
            this.setState({ groupPosts: newPosts });
          });
        }
      });
  };

  openModal = () => {
    this.setState({ createFeed: true });
  };
  closeModal = () => {
    this.setState({ createFeed: false });
  };

  render() {
    const { circle } = this.state;
    return (
      <>
      <SafeAreaView style={{flex:1,justifyContent:'flex-start' }}>
        <Modal
          animated={true}
          visible={this.state.createFeed}
          onRequestClose={this.closeModal}>
          <CreateFeeds closeModal={this.closeModal} circleId={circle.id} />
        </Modal>
        <View style={styles.container}>
          <Header icon={true} navigation={this.props.navigation} title={circle.name} />
          <ScrollView pointerEvents={'auto'}>
            <View style={styles.imageContainer}>
              <Image
                source={{ uri: circle.profile }}
                style={{ width: null, height: null, flex: 1 }}
              />
            </View>
            <View style={styles.title}>
              <Text style={styles.titleText}>{circle.name.toUpperCase()}</Text>
            </View>
            <View style={styles.description}>
              <Text style={styles.descriptionText}>{circle.description}</Text>
            </View>
            <FlatList
              data={this.state.groupPosts}
              keyExtractor={data => data.username}
              renderItem={({ item }) => <FeedCard data={item} />}
            />
          </ScrollView>
          <TouchableOpacity
            onPress={this.openModal}
            containerStyle={styles.position}>
            <View style={styles.addBtn}>
              <Text style={styles.addBtnText}
                accessible={true} accessibilityLabel={'Write Post'}
              >+</Text>
            </View>
          </TouchableOpacity>
        </View>
        </SafeAreaView >
      </>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  imageContainer: {
    height: height / 3,
    backgroundColor: '#f8f8f8',
  },

  position: {
    position: 'absolute',
    bottom: 20,
    right: 20,
  },

  addBtn: {
    width: 50,
    height: 50,
    borderRadius: 25,
    backgroundColor: colors.theme,
    justifyContent: 'center',
    alignItems: 'center',
  },

  addBtnText: {
    color: 'white',
    fontSize: 15,
  },

  title: {
    paddingHorizontal: 20,
  },

  titleText: {
    fontSize: 40,
    fontWeight: 'bold',
  },

  description: {
    paddingHorizontal: 20,
  },
  descriptionText: {
    fontSize: 15,
  },
});
