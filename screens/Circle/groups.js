import React, {useEffect, useState} from 'react';
import {View, Text, StyleSheet, TouchableOpacity,SafeAreaView, Modal} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {getCircleSuggestions} from '../../store/actions/groups';
import {FlatList} from 'react-native-gesture-handler';
import GroupCard from '../../components/GroupCard';
import colors from '../../constants/colors';
import Ionicons from 'react-native-vector-icons/Ionicons';
import SearchCircle from './SearchCircle';

const Groups = props => {
  const [state, setState] = useState({
    searchCircle: false,
  });
  const dispatch = useDispatch();
  const suggestions = useSelector(state => state.group.suggestions);
  //console.log('suggestions after all', suggestions);
  useEffect(() => {
    dispatch(getCircleSuggestions());
  }, [dispatch]);

  const clearState = () => { };

  const openModal = () => {
    setState({ ...state, searchCircle: true });
  };
  const closeModal = () => {
    setState({ ...state, searchCircle: false });
    //dispatch(getCurrentUsersCreatedGroups());
  };

  return (
    <>
    <SafeAreaView style={{flex:1,justifyContent:'flex-start'}}>
    <Modal
        animated={true}
        visible={state.searchCircle}
        onRequestClose={closeModal}>
        <SearchCircle closeModal={closeModal} />
      </Modal>
      <View style={styles.container}>
        {suggestions && suggestions.length ? (
          <FlatList
            numColumns="2"
            data={suggestions}
            keyExtractor={data => data.key}
            renderItem={data => (
              <GroupCard
                clearState={clearState}
                circle={true}
                data={data.item}
              />
            )}
          />
        ) : (
            <Text style={{ fontWeight: 'bold', fontSize: 15, alignSelf: 'center' }}>
              There is no Circle
            </Text>
          )}
        <TouchableOpacity onPress={openModal} style={styles.position} accessible={true} accessibilityLabel={'Search'}>
          <View style={styles.addBtn}>
            <Ionicons name="md-search" size={24} color="white" />
          </View>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
      
    </>
  );
};
export default Groups;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  position: {
    position: 'absolute',
    bottom: 20,
    right: 20,
  },

  addBtn: {
    width: 50,
    height: 50,
    borderRadius: 25,

    backgroundColor: colors.theme,
    justifyContent: 'center',
    alignItems: 'center',
  },

  addBtnText: {
    color: 'white',
    fontSize: 15,
  },
});
