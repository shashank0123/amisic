import React from 'react';
import { Text, View, StyleSheet, TouchableOpacity,SafeAreaView } from 'react-native';
import { SearchBar } from '../../components/SearchBar';
import colors from '../../constants/colors';
import { FlatList } from 'react-native-gesture-handler';
import GroupCard from '../../components/GroupCard';
import firestore from '@react-native-firebase/firestore';

import algoliasearch from "algoliasearch/reactnative";
import { InstantSearch } from "react-instantsearch-native";
import SearchBox from "../../components/algoliaSearchComponent/SearchBox";
import InfiniteHits from "../../components/algoliaSearchComponent/InfiniteHits";

const algoliaClient = algoliasearch(
  "KYAO8ZF1KL",
  "f8b258cfa0889ce09e7351822d5b9b76"
);

const searchClient = {
  search(requests) {
    if (requests.every(({ params }) => !params.query)) {
      // console.log(params.query);

      return Promise.resolve({
        results: requests.map(() => ({
          hits: [],
          nbHits: 0,
          nbPages: 0,
          processingTimeMS: 0,
        })),
      });
    }

    return algoliaClient.search(requests);
  },
};

export default class SearchCircle extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      suggestionVisible: false,
    };
  }
  onSearch = async searchText => {
    //console.log('searchText', searchText);
    const searchRes = await firestore()
      .collection('circles')
      .where('name', '=', searchText)
      .get();

    //console.log('searchRes', searchRes);

    if (!searchRes.empty) {
      //const data = searchRes._docs[0].data();
      this.setState({ suggestionCircles: [searchRes._docs[0].data()] });
      //console.log('data', data);
    }

    this.setState({ suggestionVisible: true });
  };
  renderSuggestions = () => {
    const { suggestionCircles } = this.state;
    //console.log();
    if (!suggestionCircles || suggestionCircles.length < 1) {
      return (
        <Text
          style={{
            textAlign: 'center',
            marginLeft: 5,
            marginBottom: 5,
            color: colors.theme,
            fontWeight: 'bold',
          }}>
          No suggestions found!
        </Text>
      );
    }
    return (

      <View style={styles.suggestionsBox}>
        <Text
          style={{
            textAlign: 'center',
            marginLeft: 5,
            marginBottom: 5,
            color: colors.theme,
            fontWeight: 'bold',
          }}>
          Search Result
        </Text>
        <FlatList
          numColumns="2"
          data={suggestionCircles}
          keyExtractor={data => data.key}
          renderItem={data => <GroupCard circle={true} data={data.item} />}
        />
      </View>
    );
  };
  render() {
    const { suggestionVisible } = this.state;
    const { closeModal } = this.props;
    const root = {
      Root: View,
      props: {
        style: {
          flex: 1,
        },
      },
    };
    return (
      <>
      <SafeAreaView style={{flex:1,justifyContent:'flex-start' }}>
        <TouchableOpacity onPress={closeModal}>
          <View style={{ alignItems: 'flex-end', paddingRight: 10, paddingTop: 10 }}>
            <Text style={{ fontWeight: 'bold', fontSize: 18, color: colors.theme }}>Close</Text>
          </View>
        </TouchableOpacity>
        <InstantSearch
          searchClient={searchClient}
          indexName="circles"
          root={root}
          onSearchStateChange={searchState =>
            console.log('=====> ', searchState)
          }>
          <SearchBox />
          <InfiniteHits type="circles" closeModal={closeModal} />
        </InstantSearch>
        </SafeAreaView>
      </>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },

  imageContainer: {
    width: 200,
    height: 200,
    borderRadius: 100,
    backgroundColor: '#f8f8f8',
    alignSelf: 'center',
    overflow: 'hidden',
  },

  choosePicture: {
    alignItems: 'center',
    marginVertical: 20,
  },
  choosePictureText: {
    fontSize: 16,
    fontWeight: 'bold',
    color: 'tomato',
  },

  label: {
    fontSize: 15,
    fontWeight: 'bold',
    marginLeft: 5,
    marginBottom: 5,
  },

  formGroup: {
    marginHorizontal: 20,
    marginBottom: 10,
  },
  buttonText: {
    color: colors.theme,
    fontWeight: 'bold',
  },
  suggestionsBox: {
    flex: 1,
  },
});
