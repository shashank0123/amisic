import React, { useState, useEffect } from 'react';
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  StatusBar,
  Alert,
  Modal,
  Image,
} from 'react-native';

import AsyncStorage from '@react-native-community/async-storage';

import Input from '../components/input';
import { useNavigation } from '@react-navigation/native';
import Button from '../components/button';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import { useDispatch, useSelector } from 'react-redux';
import { login, forgetPassword, onAuthStateChange } from '../store/actions/Auth';
import Header from '../components/header';

import colors from '../constants/colors';

const { height } = Dimensions.get('window');
const Login = props => {
  const [state, setState] = useState({
    email: '',
    password: '',
    forgetPass: false,
    splash: true,
  });

  const dispatch = useDispatch();
  const navigation = useNavigation();
  const auth = useSelector(state => state.auth);
  const onChangeEmail = text => {
    setState({ ...state, email: text });
  };

  const onChangePassword = text => {
    setState({ ...state, password: text });
  };

  const onLogin = () => {
    const { email, password } = state;
    const expression = /(?!.*\.{2})^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([\t]*\r\n)?[\t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([\t]*\r\n)?[\t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;

    if (!email) {
      return Alert.alert('Sorry!', 'Email is required');
    }

    if (!expression.test(email)) {
      return Alert.alert('Sorry!', 'Email is not valid');
    }
    if (!password) {
      return Alert.alert('Sorry!', 'Password is required');
    }

    if (!password && !email) {
      return Alert.alert('Sorry!', 'Email & Password is required');
    }
    const credential = {
      email,
      password,
    };

    dispatch(login(credential, navigation));
  };

  const navigateTo = screen => {
    navigation.navigate(screen);
  };

  const onForgetPass = () => {
    dispatch(forgetPassword(state.email));
  };

  const openModal = () => {
    setState({ ...state, forgetPass: true });
  };
  const closeModal = () => {
    setState({ ...state, forgetPass: false });
  };

  const fetchCurrentUser = async () => {
    // const user = await AsyncStorage.getItem('user');
    // if (user) {
    //   dispatch(autoAuth(JSON.parse(user)));
    //   navigation.navigate('Home');
    // }
  };
  useEffect(() => {
    // fetchCurrentUser();
    dispatch(onAuthStateChange(navigation));
    setTimeout(() => setState(states => ({ ...states, splash: false })), 7000);
  }, [dispatch, navigation]);

  return (!props.route.params || !props.route.params.noSplash) &&
    state.splash ? (
      <View style={styles.fullScreen}>
        <Image
          style={styles.splashStyle}
          source={require('../assets/images/splash.png')}
        />
      </View>
    ) : (
      <>
        <Modal
          onRequestClose={closeModal}
          visible={state.forgetPass}
          animated={true}>
          <Header title="Forget Password" />
          <View
            pointerEvents={auth.loading ? 'none' : 'auto'}
            style={styles.inputContainer}>
            <View style={styles.formGroup}>
              <Text style={styles.label}>Email*</Text>
              <Input
                myStyles={{ flex: 0 }}
                placeholder="AbhishekSingh@gmail.com"
                value={state.email}
                onChangeText={onChangeEmail}
              />
            </View>
            <Button
              myStyles={{ backgroundColor: colors.theme, marginTop: 10 }}
              loading={auth.loading}
              loadingColor="white"

              onPress={onForgetPass}
              title="Continue"
            />
          </View>
        </Modal>

        <View
          pointerEvents={auth.loading ? 'none' : 'auto'}
          style={styles.container}>
          <ScrollView keyboardShouldPersistTaps="always" style={{ flex: 1 }}>
            <StatusBar backgroundColor={colors.theme} barStyle="light-content" />
            {/* <Header icon={false} title="Login" /> */}
            <View style={styles.loginHeader}>
              <Text style={styles.loginHeaderText}>Login</Text>
            </View>
            <View style={styles.inputContainer}>
              <View style={styles.logoContainer}>
                <Image
                  source={require('../assets/images/splash.png')}
                  style={{ width: null, height: null, flex: 1 }}
                />
              </View>
              <View style={styles.formGroup}>
                <Text style={styles.label}>Email*</Text>
                <Input
                  myStyles={{ backgroundColor: 'white' }}
                  placeholder="AbhishekSingh@gmail.com"
                  value={state.email}
                  onChangeText={onChangeEmail}
                />
              </View>
              <View style={styles.formGroup}>
                <Text style={styles.label}>Password*</Text>
                <Input
                  myStyles={{ backgroundColor: 'white' }}
                  secureTextEntry={true}
                  placeholder="enter password here"
                  value={state.password}
                  onChangeText={onChangePassword}
                />
              </View>
            </View>
            <View style={{ flex: 1 }}>
              <Button
                loading={auth.loading}
                loadingColor="white"
                textStyle={{ color: 'white' }}
                myStyles={{ backgroundColor: colors.theme, marginTop: 10 }}
                onPress={onLogin}
                title="Login"
              />

              <View style={styles.dontHaveAccount}>
                <Text style={{ fontSize: 15 }}>
                  Don't have an account, create one
              </Text>
                <TouchableOpacity onPress={() => navigateTo('Signup')}>
                  <Text style={{ fontWeight: 'bold', marginLeft: 10 }}>Signup</Text>
                </TouchableOpacity>
              </View>
              <View style={styles.forgetPassContainer}>
                <TouchableOpacity onPress={openModal}>
                  <Text style={styles.forgetText}>Forget Password</Text>
                </TouchableOpacity>
              </View>
            </View>
          </ScrollView>
        </View>
      </>
    );
};
export default Login;

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#f8f8f8',
    zIndex: 10,
    flex: 1,
  },

  label: {
    fontSize: 15,
    fontWeight: 'bold',
    marginLeft: 5,
    marginBottom: 5,
  },

  formGroup: {
    marginHorizontal: 20,
    marginBottom: 10,
  },
  loginHeader: {
    height: height / 5,
    backgroundColor: colors.theme,
    justifyContent: 'flex-end',
    paddingHorizontal: 20,
  },

  loginHeaderText: {
    fontSize: 35,
    color: 'white',
    marginBottom: 10,
    fontWeight: 'bold',
  },

  inputContainer: {
    marginVertical: 20,
  },
  logoContainer: {
    height: 150,
    width: 150,
    backgroundColor: 'white',
    alignSelf: 'center',
    marginBottom: 20,
  },

  logoText: {
    fontSize: 40,
    color: 'black',
    fontWeight: 'bold',
    textAlign: 'center',
  },

  dontHaveAccount: {
    margin: 20,
    flexDirection: 'row',
  },

  forgetPassContainer: {
    paddingHorizontal: 20,
  },

  forgetText: {
    fontSize: 15,
    fontWeight: 'bold',
  }, fullScreen: {
    height: '100%',
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  splashStyle: {
    width: '100%',
    height: '100%',
    resizeMode: 'contain',
  },
});
