import React from 'react';
import { View, Text, FlatList, SafeAreaView, StyleSheet } from 'react-native';
import Header from '../components/header';
import MessageCard from '../components/messageCard';
import firestore from '@react-native-firebase/firestore';
import auth from '@react-native-firebase/auth';
import { getUserDetailsById } from '../utils';
import { TouchableOpacity } from 'react-native-gesture-handler';
import colors from '../constants/colors';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { SearchBar } from '../components/SearchBar';

class Message extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      conversations: [],
      friends: [],
      searchVisible: false,
    };
  }
  componentDidMount = async () => {
    const loggedInId = auth().currentUser.uid;
    let { conversations } = this.state;

    await firestore()
      .collection('conversations')
      .where('memberIds', 'array-contains', loggedInId)
      .onSnapshot(snapshot => {
        if (!snapshot.empty) {
          snapshot._docs.forEach(async doc => {
            const { recentMessage, memberIds, id } = doc.data();
            let senderId = '';
            if (memberIds[0] === loggedInId) {
              senderId = memberIds[1];
            } else {
              senderId = memberIds[0];
            }
            const senderDetails = await getUserDetailsById(senderId);
            const { fullname, profile } = senderDetails.data;

            //check if it is a change in eixisting conversation, or a new conversation is fetched
            if (conversations.some(conversation => conversation.id === id)) {
              //existing conversation
              let index = conversations.indexOf(
                conversations.filter(conversation => conversation.id === id)[0],
              );
              conversations[index] = {
                id,
                senderName: fullname,
                senderProfile: profile,
                message: recentMessage,
                senderId,
              };
            } else {
              //new conversation
              conversations.push({
                id,
                senderName: fullname,
                senderProfile: profile,
                message: recentMessage,
                senderId,
              });
            }
            this.setState({ conversations });
          });
        }
      });

    let friends = [];
    const friendres = await firestore()
      .collection('users')
      .doc(loggedInId)
      .get();
    if (friendres.exists) {
      const { friendIds } = friendres.data();
      console.log('friendIds', friendIds);
      if (friendIds) {
        let result = friendIds.map(async friendId => {
          const userRes = await firestore()
            .collection('users')
            .doc(friendId)
            .get();
          const { userId, fullname, profile } = userRes.data();

          return { userId, fullname, profile };
        });

        Promise.all(result).then(completed => {
          completed.forEach(item => {
            const { userId, fullname, profile } = item;
            friends.push({
              userId,
              senderName: fullname,
              senderProfile: profile,
              //message: recentMessage,
              //senderId,
            });
            //conversations.push({ userId, fullname, profile });
          });
          this.setState({ friends });
        });
      }
    }



  };
  onSearch = async searchText => {
    let { searchConversations, conversations } = this.state;
    if (searchText) {
      const regex = new RegExp(`${searchText.trim()}`, 'i');
      searchConversations = conversations && conversations.length > 0 ? conversations.filter(x => x.senderName.search(regex) >= 0) : [];
      this.setState({ searchConversations });
    } else {
      searchConversations = []
      this.setState({ searchConversations });
    }



    // const loggedInId = auth().currentUser.uid;
    // const searchres = await firestore()
    //   .collection('conversations')
    //   .where('senderName', '=', searchText)
    //   .get();
    // if (!searchres.empty) {
    //   const {memberIds, id, recentMessage} = searchres._docs[0].data();
    //   let senderId = '';
    //   if (memberIds[0] === loggedInId) {
    //     senderId = memberIds[1];
    //   } else {
    //     senderId = memberIds[0];
    //   }
    //   const senderDetails = await getUserDetailsById(senderId);
    //   const {fullname, profile} = senderDetails.data;
    //   const searchConversations = [
    //     {
    //       id,
    //       senderName: fullname,
    //       senderProfile: profile,
    //       message: recentMessage,
    //       senderId,
    //     },
    //   ];

    //   this.setState({searchConversations});
    // }
  };

  toggleSearchVisible = () => {
    this.setState({
      searchConversations: [],
      searchVisible: !this.state.searchVisible,
    });
  };

  render() {
    const { conversations, searchConversations, searchVisible } = this.state;
    console.log(conversations)
    return (
      <SafeAreaView style={{flex:1,justifyContent:'flex-start'}}>
        <View style={styles.container}>
        <Header icon={false} title="Messages" />
        {!searchVisible ? (
          <FlatList
            data={conversations}
            renderItem={item => (
              <MessageCard
                username={item.item.senderName}
                userProfile={item.item.senderProfile}
                text={item.item.message}
                conversationId={item.item.id}
                senderId={item.item.senderId}
              />
            )}
          />
        ) : (
            <>
              <TouchableOpacity onPress={this.toggleSearchVisible}>
                <View style={{ alignItems: 'flex-end', paddingRight: 10, paddingTop: 10 }}>
                  <Text style={{ fontWeight: 'bold', fontSize: 18, color: colors.theme }}>Close</Text>
                </View>
              </TouchableOpacity>
              <SearchBar
                accessible={true} accessibilityLabel={'Search'}
                onSearch={this.onSearch} />
              <FlatList
                data={searchConversations}
                renderItem={item => (
                  <MessageCard
                    username={item.item.senderName}
                    userProfile={item.item.senderProfile}
                    text={item.item.message}
                    conversationId={item.item.id}
                    senderId={item.item.senderId}
                  />
                )}
              />
            </>
          )}
        <TouchableOpacity
          onPress={this.toggleSearchVisible}
          accessible={true} accessibilityLabel={'Search'}
          containerStyle={styles.position}>
          <View style={styles.addBtn}>
            <Ionicons name="md-search" size={24} color="white" />
          </View>
        </TouchableOpacity>
      </View>
      </SafeAreaView>
      
    );
  }
}
export default Message;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  position: {
    position: 'absolute',
    bottom: 20,
    right: 20,
  },

  addBtn: {
    width: 50,
    height: 50,
    borderRadius: 25,

    backgroundColor: colors.theme,
    justifyContent: 'center',
    alignItems: 'center',
  },

  addBtnText: {
    color: 'white',
    fontSize: 15,
  },
});