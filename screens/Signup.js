import React, { useState } from 'react';
import {
  View,
  Text,
  StyleSheet,
  Alert,
  StatusBar,
  Dimensions,
  CheckBox,
  Image,
} from 'react-native';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import { useNavigation } from '@react-navigation/native';
import Button from '../components/button';
import Input from '../components/input';
import { useDispatch, useSelector } from 'react-redux';
import { createAccount } from '../store/actions/Auth';
import MyPicker from '../components/Picker';
import colors from '../constants/colors';
import strings from '../constants/strings';
import firestore from '@react-native-firebase/firestore';

const { width, height } = Dimensions.get('window');

const Signup = props => {
  const [state, setState] = useState({
    email: '',
    password: '',
    fullname: '',
    confirmPassword: '',
    //securityQuestion: '',
    //securityAns: '',
    disabilityType: '',
    isDisabled: false,
  });
  const navigation = useNavigation();
  const auth = useSelector(state => state.auth);
  const dispatch = useDispatch();

  const onChangeEmail = text => {
    setState({ ...state, email: text });
  };

  const onChangePassword = text => {
    setState({ ...state, password: text });
  };

  const onChangeFullName = text => {
    setState({ ...state, fullname: text });
  };

  const onChangeCnfrmPassword = text => {
    setState({ ...state, confirmPassword: text });
  };

  // const onSecurityQuestionChange = value => {
  //   setState({...state, securityQuestion: value});
  // };

  const onChangeDisabilityType = text => {
    setState({ ...state, disabilityType: text });
  };

  const onSignUp = async () => {
    const {
      email,
      password,
      confirmPassword,
      fullname,
      // securityAns,
      // securityQuestion,
      isDisabled,
      disabilityType,
    } = state;

    let userDisabilities = [],
      tempDisability;

    const expression = /(?!.*\.{2})^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([\t]*\r\n)?[\t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([\t]*\r\n)?[\t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;

    if (!fullname) {
      return Alert.alert('Sorry!', 'Fullname is required');
    }

    if (!email) {
      return Alert.alert('Sorry!', 'Email is required');
    }

    if (expression.test('Inavalid', 'Email is not valid')) {
      return Alert.alert('Sorry!', 'Email is required');
    }
    if (!password) {
      return Alert.alert('Sorry!', 'Password is required');
    }

    if (password.length < 6) {
      return Alert.alert('Sorry!', 'Password should be more than 6 characters');
    }

    if (confirmPassword !== password) {
      return Alert.alert('Sorry!', 'Password is not match');
    }

    if (!password && !email) {
      return Alert.alert('Sorry!', 'Email & Password is required');
    }

    // if (!securityQuestion) {
    //   return Alert.alert('Sorry!', 'Please Select Security Question');
    // }

    // if (!securityAns) {
    //   return Alert.alert('Sorry!', 'Security Answer Required');
    // }

    if (isDisabled) {
      if (!disabilityType) {
        return Alert.alert('Sorry!', 'Disability type Required');
      }
      let myDisabilities = [];
      myDisabilities = disabilityType.split(',');

      const results = myDisabilities.map(async myDisability => {
        myDisability = myDisability.trim().toLowerCase();
        const res = await firestore()
          .collection('disabilities')
          .where('name', '==', myDisability)
          .get();

        if (res.empty) {
          //If it is a new disability
          const disabilityRef = firestore()
            .collection('disabilities')
            .doc();
          tempDisability = {
            id: disabilityRef.id,
            name: myDisability,
          };
          await disabilityRef.set({
            id: disabilityRef.id,
            name: myDisability,
          });
        } else {
          // if it is an existing disability
          const data = res._docs[0].data();
          tempDisability = {
            id: data.id,
            name: data.name,
          };
        }
        return tempDisability;
      });
      Promise.all(results).then(completed => {
        completed.forEach(item => {
          userDisabilities.push(item);
        });
        const credential = {
          email: email.trim(),
          password,
          fullname: fullname.trim(),
          confirmPassword,
          isDisabled,
          userDisabilities,
        };
        dispatch(createAccount(credential, navigation));
      });
      //console.log('userDisabilities', userDisabilities);
    } else {
      const credential = {
        email,
        password,
        fullname,
        confirmPassword,
        isDisabled,
        userDisabilities,
      };
      dispatch(createAccount(credential, navigation));
    }
  };

  const navigateTo = screen => {
    navigation.navigate(screen);
  };

  const toggleDisability = () => {
    const { isDisabled } = state;
    setState({ ...state, isDisabled: !isDisabled });
  };

  return (
    <ScrollView
      keyboardShouldPersistTaps="always"
      style={{ flex: 1 }}
      pointerEvents={auth.loading ? 'none' : 'auto'}>
      <View style={styles.container}>
        <StatusBar backgroundColor={colors.theme} barStyle="light-content" />
        {/* <Header icon={false} title="Login" /> */}
        <View style={styles.loginHeader}>
          <Text style={styles.loginHeaderText}>Signup</Text>
        </View>
        <View style={styles.inputContainer}>
          <View style={styles.logoContainer}>
            <Image
              source={require('../assets/images/logo.png')}
              style={{ width: null, height: null, flex: 1 }}
            />
          </View>
          <View style={styles.formGroup}>
            <Text style={styles.label}>Fullname*</Text>
            <Input
              myStyles={{ backgroundColor: 'white' }}
              placeholder="Abhishek Singh"
              value={state.fullname}
              onChangeText={onChangeFullName}
            />
          </View>
          <View style={styles.formGroup}>
            <Text style={styles.label}>Email*</Text>
            <Input
              myStyles={{ backgroundColor: 'white' }}
              placeholder="AbhishekSingh@gmail.com"
              value={state.email}
              onChangeText={onChangeEmail}
            />
          </View>

          <View style={styles.formGroup}>
            <Text style={styles.label}>Password*</Text>
            <Input
              myStyles={{ backgroundColor: 'white' }}
              secureTextEntry={true}
              placeholder="enter password here"
              value={state.password}
              onChangeText={onChangePassword}
            />
          </View>
          <View style={styles.formGroup}>
            <Text style={styles.label}>Confirm Password*</Text>
            <Input
              myStyles={{ backgroundColor: 'white' }}
              secureTextEntry={true}
              placeholder="Confirm password"
              value={state.confirmPassword}
              onChangeText={onChangeCnfrmPassword}
            />
          </View>

          <View style={styles.formGroup}>
            <View
              style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
              <Text style={styles.label}>{strings.are_disabled}</Text>
              <CheckBox
                value={state.isDisabled}
                style={styles.checkbox}
                onValueChange={toggleDisability}
              />
            </View>
            {/* <MyPicker
              myStyles={{backgroundColor: 'white'}}
              value={state.securityQuestion}
              onValueChange={onSecurityQuestionChange}
            /> */}
          </View>
          {state.isDisabled && (
            <View style={styles.formGroup}>
              <Input
                myStyles={{ backgroundColor: 'white' }}
                placeholder={strings.disability_type}
                value={state.disabilityType}
                onChangeText={onChangeDisabilityType}
              />
            </View>
          )}
          <Button
            loading={auth.loading}
            loadingColor="white"
            textStyle={{ color: 'white' }}
            myStyles={{ backgroundColor: colors.theme, marginTop: 20 }}
            onPress={onSignUp}
            title="Signup"
          />

          <View style={styles.dontHaveAccount}>
            <Text style={{ fontSize: 15 }}>Already have an account, login</Text>
            <TouchableOpacity onPress={() => navigateTo('Login')}>
              <Text style={{ fontWeight: 'bold', marginLeft: 10 }}>Login</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </ScrollView>
  );
};
export default Signup;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f8f8f8',
  },

  label: {
    fontSize: 15,
    fontWeight: 'bold',
    marginLeft: 5,
    marginBottom: 5,
  },

  formGroup: {
    marginHorizontal: 20,
    marginBottom: 10,
  },
  loginHeader: {
    height: height / 5,
    backgroundColor: colors.theme,
    justifyContent: 'flex-end',
    paddingHorizontal: 20,
  },

  loginHeaderText: {
    fontSize: 35,
    color: 'white',
    marginBottom: 10,
    fontWeight: 'bold',
  },

  inputContainer: {
    marginVertical: 20,
  },
  logoContainer: {
    height: 150,
    width: 150,
    backgroundColor: 'white',
    alignSelf: 'center',
    marginBottom: 20,
  },

  logoText: {
    fontSize: 40,
    color: 'black',
    fontWeight: 'bold',
    textAlign: 'center',
  },

  dontHaveAccount: {
    margin: 20,
    flexDirection: 'row',
  },
});
