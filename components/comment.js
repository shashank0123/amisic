import React from 'react';
import { View, StyleSheet, Alert } from 'react-native';
import Input from './input';
import Feather from 'react-native-vector-icons/Feather';
import { TouchableOpacity } from 'react-native-gesture-handler';
import colors from '../constants/colors';
import firestore from '@react-native-firebase/firestore';
import { getUserDetailsById } from '../utils';

class Comment extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      commentText: '',
    };
  }

  onCommentChange = commentText => {
    this.setState({
      commentText,
    });
  };

  onSend = async () => {
    //Alert.alert('Sent');
    const { commentText } = this.state;
    const { toggleComment, userId, postId } = this.props;
    toggleComment();
    const userRes = await getUserDetailsById(userId);

    let batch = firestore().batch();

    let commentRef = firestore()
      .collection('comments')
      .doc();
    let postRef = firestore()
      .collection('posts')
      .doc(postId);

    batch.update(postRef, {
      commentIds: firestore.FieldValue.arrayUnion(commentRef.id),
    });

    batch.set(commentRef, {
      id: commentRef.id,
      postId: postId,
      text: commentText,
      replyIds: [],
      userId: userId,
      userName: userRes.data.fullname,
      profile: userRes.data.profile,
      // createdAt: firestore.FieldValue.serverTimestamp(),
      createdAt: firestore.FieldValue.serverTimestamp(),
    });
    batch.commit();
    //await fire
  };
  render() {
    const { commentText } = this.state;
    return (
      <View style={styles.container}>
        <Input
          myStyles={{ minHeight: 40, flex: 1 }}
          placeholder="enter comment"
          multiline={true}
          placeholderTextColor="grey"
          onChangeText={this.onCommentChange}
          value={commentText}
        />
        <TouchableOpacity onPress={this.onSend}>
          <Feather
            style={{ marginLeft: 10 }}
            size={22}
            color={colors.theme}
            name="send"
          />
        </TouchableOpacity>
      </View>
    );
  }
}
export default Comment;

const styles = StyleSheet.create({
  container: {
    height: 50,
    flexDirection: 'row',
    paddingHorizontal: 20,
    alignItems: 'center',
    marginVertical: 10,
  },
});
