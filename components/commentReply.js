import React from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity } from 'react-native';
import firestore from '@react-native-firebase/firestore';
import moment from 'moment';
import { getUserDetailsById } from '../utils';

class Reply extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      reply: '',
    };
  }

  componentDidMount = async () => {
    const { replyId } = this.props;
    const replyRes = await firestore()
      .collection('replies')
      .doc(replyId)
      .get();
    const { text, userId, createdAt } = replyRes.data();
    const userData = await getUserDetailsById(userId);
    const { fullname, profile } = userData.data;
    this.setState({
      createdAt,
      reply: text,
      userName: fullname,
      profilePic: profile,
    });
  };
  render() {
    const { reply, userName, profilePic, createdAt } = this.state;
    const { closeModal, navigation } = this.props;
    return (
      <View style={styles.replyContainer}>
        <TouchableOpacity
          onPress={() => {
            closeModal();
            navigation.navigate('FriendsProfile', {
              profile: {
                fullname: userName,
                profile: profilePic,
                noButton: true,
              },
            });
          }}
          style={styles.replyAvatar}>
          <Image
            source={{
              uri: profilePic,
            }}
            style={styles.replyAvatar}
          />
        </TouchableOpacity>
        <View style={styles.commentInfo}>
          <TouchableOpacity
            onPress={() => {
              closeModal();
              navigation.navigate('FriendsProfile', {
                profile: {
                  fullname: userName,
                  profile: profilePic,
                  noButton: true,
                },
              });
            }}>
            <Text style={styles.username}>{userName}</Text>
          </TouchableOpacity>
          <Text style={styles.replyText}>{reply}</Text>
          {createdAt && (
            <Text style={styles.commentInfo}>
              {moment(new Date(createdAt.seconds * 1000)).fromNow()}
            </Text>
          )}
          {/* <TouchableOpacity>
            <Text style={{fontSize: 14, fontWeight: 'bold'}}>Reply</Text>
          </TouchableOpacity> */}
        </View>
      </View>
    );
  }
}
export default Reply;

const styles = StyleSheet.create({
  replyAvatar: {
    width: 30,
    height: 30,
    borderRadius: 15,
    backgroundColor: '#f8f8f8',
    overflow: 'hidden',
    marginRight: 5,
  },

  replyContainer: {
    paddingHorizontal: 35,
    flexDirection: 'row',
    marginVertical: 5,
  },

  username: {
    fontSize: 12,
    fontWeight: 'bold',
  },
  replyText: {
    fontSize: 14,
    backgroundColor: '#f8f8f8',
    padding: 10,
  },
});
