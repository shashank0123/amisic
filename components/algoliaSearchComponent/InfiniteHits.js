import React from 'react';
import { StyleSheet, Text, View, FlatList } from 'react-native';
import PropTypes from 'prop-types';
import { connectInfiniteHits } from 'react-instantsearch-native';
import Highlight from './Highlight';
import GroupCard from '../../components/GroupCard';
import AddFriendCard from '../../components/AddFriendCard';

const styles = StyleSheet.create({
  separator: {
    borderBottomWidth: 1,
    borderColor: '#ddd',
  },
  item: {
    padding: 10,
    flexDirection: 'column',
  },
  titleText: {
    fontWeight: 'bold',
  },
});

const InfiniteHits = ({ hits, hasMore, refine, closeModal, type }) => {
  return (
    hits.length > 0 ?
      <FlatList
        numColumns={2}
        data={hits}
        keyExtractor={item => item && item.objectID}
        ItemSeparatorComponent={() => <View style={styles.separator} />}
        onEndReached={() => hasMore && refine()}
        renderItem={({ item }) => (
          <>
            {
              item ?
                <>
                  {
                    type == 'circles' ? <GroupCard circle={true} data={{ ...item, closeModal: closeModal }} /> : null
                  }
                  {
                    type == 'users' ? <AddFriendCard data={{ ...item, closeModal: closeModal }} /> : null
                  }
                </>
                : null
            }
          </>



          // <View  style={styles.item}>
          //   <Text>{ item && item.fullname ? item.fullname : ''}</Text>
          // </View>
        )}
      />
      : null
  )
  //return(


  //   <FlatList
  //   data={hits}
  //   keyExtractor={item => item.objectID}
  //   ItemSeparatorComponent={() => <View style={styles.separator} />}
  //   onEndReached={() => hasMore && refine()}
  //   renderItem={({ item }) => (
  //     <View style={styles.item}>
  //       <Highlight attribute="name" hit={item} />
  //     </View>
  //   )}
  // />

  //)
}

InfiniteHits.propTypes = {
  hits: PropTypes.arrayOf(PropTypes.object).isRequired,
  hasMore: PropTypes.bool.isRequired,
  refine: PropTypes.func.isRequired,
};

export default connectInfiniteHits(InfiniteHits);
