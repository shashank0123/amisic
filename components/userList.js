import React from 'react';
import { View, TouchableOpacity, Text, StyleSheet, Image, SafeAreaView } from 'react-native';
import firestore from '@react-native-firebase/firestore';
import ReplyBox from './ReplyBox';
import Reply from './commentReply';
import { getUserDetailsById } from '../utils';
import colors from '../constants/colors';

export default class UserList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      users: [],
    };
  }

  componentDidMount = async () => {
    const { userIds } = this.props;
    console.log('userIds', userIds);
    const result = userIds.map(async userId => {
      const userDetails = await getUserDetailsById(userId);
      const { fullname, profile } = userDetails.data;
      return { fullname, profile };
    });
    let users = [];

    Promise.all(result).then(completed => {
      completed.forEach(item => {
        users.push(item);
      });
      this.setState({ users });
    });
  };

  render() {
    const { users } = this.state;
    const { navigation, closeModal } = this.props;
    return (
      <>
      <SafeAreaView style={{flex:1,justifyContent:'flex-start', paddingTop: 20  }}>
        <View style={{ flexDirection: 'row' }}>
          <View style={{ flex: 1 }}>
            <Text style={{ fontWeight: 'bold', marginVertical: 10, marginLeft: 10 }}>
              {'Likes: '}
            </Text>
          </View>
          <View style={{ flex: 1 }}>
            <TouchableOpacity onPress={closeModal}>
              <View style={{ alignItems: 'flex-end', paddingRight: 10, paddingTop: 10 }}>
                <Text style={{ fontWeight: 'bold', fontSize: 18, color: colors.theme }}>Close</Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>

        {users.map(user => {
          return (
            <TouchableOpacity
              onPress={() => {
                closeModal();
                navigation.navigate('FriendsProfile', {
                  profile: {
                    fullname: user.fullname,
                    profile: user.profile,
                    noButton: true,
                  },
                });
              }}
              style={styles.listItem}>
              <View style={styles.imageContainer}>
                <Image
                  source={{ uri: user.profile }}
                  style={styles.imageContainer}
                />
              </View>
              <View>
                <Text style={styles.username}>{user.fullname}</Text>
              </View>
            </TouchableOpacity>
          );
        })}
        </SafeAreaView>
      </>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    paddingHorizontal: 10,
  },
  listItem: {
    height: 70,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#fff',
    borderBottomColor: 'rgba(0,0,0,.1)',
    borderBottomWidth: 1,
    paddingHorizontal: 20,
  },
  imageContainer: {
    width: 40,
    height: 40,
    borderRadius: 20,
    backgroundColor: '#f8f8f8',
    marginRight: 10,
  },
  username: {
    fontSize: 14,
    fontWeight: 'bold',
  },
});
