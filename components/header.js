import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import Ionicons from 'react-native-vector-icons/Ionicons';
const Header = ({ navigation, title, myStyle, myheaderTitleStyles, icon }) => {
  return (
    <View
      style={{
        ...styles.container,
        ...myStyle,
        // justifyContent: icon ? 'flex-start' : 'center',
        marginLeft: icon ? 10 : 0,
      }}>
      {icon ? (
        <TouchableOpacity onPress={() => navigation.goBack()}>
          <Ionicons name="md-arrow-back" size={24} color="black" />
        </TouchableOpacity>
      ) : null}

      <Text style={{ ...styles.headerTitle, ...myheaderTitleStyles }}>
        {title}
      </Text>
    </View>
  );
};
export default Header;

const styles = StyleSheet.create({
  container: {
    height: 60,
    backgroundColor: '#fff',
    alignItems: 'center',
    paddingHorizontal: 20,
    flexDirection: 'row',
    //elevation: 5,
  },

  headerTitle: {
    fontSize: 18,
    fontWeight: '700',
    flex: 1,
    textAlign: 'center'
  },
});
