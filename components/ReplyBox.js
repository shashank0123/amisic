import React from 'react';
import { View, StyleSheet, TouchableOpacity, Alert } from 'react-native';
import Input from './input';
import Feather from 'react-native-vector-icons/Feather';
import colors from '../constants/colors';
import firestore from '@react-native-firebase/firestore';
import { getUserDetailsById } from '../utils';

class ReplyBox extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      replyText: '',
    };
  }

  onReplyChange = replyText => {
    this.setState({
      replyText,
    });
  };

  onSend = async () => {
    //Alert.alert('Sent');
    console.log('sending reply');
    const { replyText } = this.state;
    const { toggleReply, userId, commentId } = this.props;
    const userRes = await getUserDetailsById(userId);
    const { fullname, profile } = userRes.data;
    toggleReply(commentId);

    let batch = firestore().batch();

    let replyRef = firestore()
      .collection('replies')
      .doc();
    let commentRef = firestore()
      .collection('comments')
      .doc(commentId);

    batch.update(commentRef, {
      replyIds: firestore.FieldValue.arrayUnion(replyRef.id),
      recentReply: {
        userName: fullname,
        profilePic: profile,
        reply: replyText,
        createdAt: firestore.FieldValue.serverTimestamp(),
      },
    });

    batch.set(replyRef, {
      id: replyRef.id,
      commentId: commentId,
      text: replyText,
      //replyIds: [],
      userId: userId,
      createdAt: firestore.FieldValue.serverTimestamp(),
    });
    batch.commit();
    //await fire
  };
  render() {
    const { replyText } = this.state;
    return (
      <View style={styles.container}>
        <Input
          myStyles={{ minHeight: 40, flex: 1 }}
          placeholder="Enter Reply"
          multiline={true}
          placeholderTextColor="grey"
          onChangeText={this.onReplyChange}
          value={replyText}
        />
        <TouchableOpacity onPress={this.onSend}>
          <Feather
            style={{ marginLeft: 10 }}
            size={22}
            color={colors.theme}
            name="send"
          />
        </TouchableOpacity>
      </View>
    );
  }
}
export default ReplyBox;

const styles = StyleSheet.create({
  container: {
    height: 50,
    flexDirection: 'row',
    paddingHorizontal: 20,
    alignItems: 'center',
    marginVertical: 10,
  },
});
