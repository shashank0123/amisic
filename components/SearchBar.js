import React, { useState } from 'react';
import { Text, View, StyleSheet, TouchableOpacity, SafeAreaView } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Input from './input';

export const SearchBar = props => {
  const [state, setState] = useState({
    searchText: '',
  });

  const searchTextChange = text => {
    props.onSearch(text)
    setState({ ...state, searchText: text });
  };
  return (
    <SafeAreaView style={{flex:1,justifyContent:'flex-start' }}>
    <View style={styles.searchBar}>
      <TouchableOpacity
        onPress={() => props.onSearch(state.searchText)}
        style={styles.lens}>
        <Ionicons name="md-search" size={30} color="gray" />
      </TouchableOpacity>
      <Input
        myStyles={styles.border}
        onChangeText={searchTextChange}
        value={state.searchText}
      />
    </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  searchBar: {
    margin: 5,
    width: '97%',
    height: 50,
  },
  border: {
    borderWidth: 2,
    borderColor: 'gray',
  },
  lens: {
    position: 'absolute',
    zIndex: 1,
    right: 10,
    top: 8,
  },
});
