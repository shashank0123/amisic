import React from 'react';
import {
  View,
  TouchableOpacity,
  Text,
  StyleSheet,
  Dimensions,
  Image,
} from 'react-native';
import colors from '../constants/colors';
import { useNavigation } from '@react-navigation/native';

const { width, height } = Dimensions.get('window');

const AddFriendCard = ({ data }) => {
  const navigation = useNavigation();

  const navigateTo = (route, profile) => {
    if (profile.closeModal) {
      profile.closeModal();
    }
    navigation.navigate(route, { profile });
  };
  return (
    <TouchableOpacity onPress={() => navigateTo('FriendsProfile', { ...data })}>
      <View style={styles.container}>
        <View style={styles.imageContainer}>
          <Image
            source={{
              uri: data.profile
                ? data.profile
                : 'https://i.ya-webdesign.com/images/instagram-profile-photo-png-2.png',
            }}
            style={{ flex: 1 }}
          />
        </View>

        <View style={styles.profileInfo}>
          <Text style={styles.username}>{data.fullname}</Text>
        </View>
        {/*
        <TouchableOpacity style={styles.btn}>
          <Text style={styles.btnText}>Add Friend</Text>
        </TouchableOpacity> */}
      </View>
    </TouchableOpacity>
  );
};
export default AddFriendCard;

const styles = StyleSheet.create({
  container: {
    width: width / 2 - 10,
    height: height / 3.7,
    backgroundColor: 'white',
    margin: 5,
    borderRadius: 8,
    justifyContent: 'center',
  },

  imageContainer: {
    marginVertical: 10,
    width: 80,
    height: 80,
    borderRadius: 40,
    overflow: 'hidden',
    backgroundColor: '#f8f8f8',
    alignSelf: 'center',
  },

  profileInfo: {
    marginBottom: 10,
    alignSelf: 'center',
  },

  btn: {
    padding: 5,
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: 20,
    borderRadius: 10,
    backgroundColor: colors.theme,
    elevation: 3,
  },

  btnText: {
    fontSize: 13,
    fontWeight: 'bold',
    color: 'white',
  },

  username: {
    fontWeight: 'bold',
    fontSize: 16,
  },
});
