import React, { useState } from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  Dimensions,
  SafeAreaView,  Modal,
  Share,
  Alert,
} from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';

import Video from 'react-native-video';

import AntDesign from 'react-native-vector-icons/AntDesign';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import Entypo from 'react-native-vector-icons/Entypo';
import { useSelector } from 'react-redux';
import Comment from '../components/comment';
import moment from 'moment';
import CommentList from './commentList';
import firestore from '@react-native-firebase/firestore';
import AllComments from './allComments';
import { useNavigation } from '@react-navigation/native';
import UserList from './userList';
const { height } = Dimensions.get('window');

const FeedCard = ({ data }) => {
  const [state, setState] = useState({
    comment: false,
    likeUserIds: data.likeUserIds,
    isLiked: data.isLiked,
    modalVisible: false,
    likerModalVisible: false,
  });
  const auth = useSelector(state => state.auth);
  const navigation = useNavigation();
  const navigateTo = (route, profile) => {
    navigation.navigate(route, { profile });
  };
  //console.log('isliked', state.isLiked);

  const toggleComment = () => {
    setState({ ...state, comment: !state.comment });
  };

  // const setParentState==>{
  //   this.setState({...state,})
  // }

  const closeModal = () => {
    setState({ ...state, modalVisible: false });
  };

  const closeLikerModal = () => {
    setState({ ...state, likerModalVisible: false });
  };

  const onMorePressed = () => {
    setState({ ...state, modalVisible: true });
  };

  // const shareBtn = async () => {
  //   try {
  //     const result = await Share.share({
  //       message:
  //         'React Native | A framework for building native apps using React',
  //     });
  //     if (result.action === Share.sharedAction) {
  //       if (result.activityType) { alert('shared1')
  //         // shared with activity type of result.activityType
  //       } else {alert('shared2')
  //         // shared
  //       }
  //     } else if (result.action === Share.dismissedAction) {alert(3)
  //       // dismissed
  //     }
  //   } catch (error) {
  //     alert(error.message);
  //   }
  // }

  const onLikePressed = async () => {
    let { likeUserIds } = state;
    if (state.isLiked) {
      const index = likeUserIds.indexOf(auth.user.userId);
      likeUserIds.splice(index, 1);
      setState({ ...state, likeUserIds, isLiked: false });
    } else {
      likeUserIds.push(auth.user.userId);
      setState({ ...state, likeUserIds, isLiked: true });
    }
    await firestore()
      .collection('posts')
      .doc(data.id)
      .update({
        likeUserIds,
      });
  };
  return (
    <SafeAreaView style={{flex:1,justifyContent:'flex-start', paddingTop: 20  }}>
    <View style={styles.container}>
      <View style={styles.userInfo}>
        <TouchableOpacity
          accessible={true} accessibilityLabel={'Image'}
          onPress={() =>
            navigateTo('FriendsProfile', {
              fullname: data.userName,
              profile: data.profilePic,
              email: data.email,
              userDisabilities: data.userDisabilities,
              noButton: true,
            })
          }
          style={styles.user_image}>
          <Image source={{ uri: data.profilePic }} style={styles.user_image} />
        </TouchableOpacity>
        <TouchableOpacity
          // accessible={true} accessibilityLabel={'Friends Profile Navigation'}
          onPress={() =>
            navigateTo('FriendsProfile', {
              fullname: data.userName,
              profile: data.profilePic,
              email: data.email,
              userDisabilities: data.userDisabilities,
              noButton: true,
            })
          }>
          <Text style={styles.userName}>{data.userName}</Text>
        </TouchableOpacity>
      </View>
      {data.image && (
        <View style={styles.imageContainer}>
          <Image
            source={{
              uri: data.image,
            }}
            style={styles.image}
          />
        </View>
      )}
      {data.Video && (
        <View style={styles.imageContainer}>
          <Video
            source={{ uri: data.video }}
            style={{ flex: 1, width: null, height: null }}
            controls={true}
          />
        </View>
      )}
      <View style={styles.description}>
        <Text style={styles.descriptionText}>{data.description}</Text>
      </View>
      <Text style={{ paddingLeft: 10 }}>
        {moment(new Date(data.createdAt.seconds * 1000)).fromNow()}
      </Text>

      <View style={styles.likesContainer}>
        <TouchableOpacity
          disabled={!state.likeUserIds || state.likeUserIds.length === 0}
          onPress={() => setState({ ...state, likerModalVisible: true })}>
          <Text style={styles.likeText}>
            {'Likes ' +
              (state.likeUserIds && state.likeUserIds.length > 0
                ? state.likeUserIds.length
                : 0)}
          </Text>
        </TouchableOpacity>
      </View>

      <View style={styles.actionContainer}>
        <View style={styles.likeCommentBtn}>
          <TouchableOpacity
            onPress={() => onLikePressed()}
            containerStyle={styles.btn}>
            <Text>
              Like{' '}
              <AntDesign
                //name="like1"
                name={state.isLiked ? 'like1' : 'like2'}
                size={16}
                color="black"
              />
            </Text>
          </TouchableOpacity>
        </View>
        <View style={styles.likeCommentBtn}>
          <TouchableOpacity onPress={toggleComment} containerStyle={styles.btn}>
            <Text>
              Comment <EvilIcons name="comment" size={16} color="black" />
            </Text>
          </TouchableOpacity>
        </View>
        <View style={styles.likeCommentBtn}>
          <TouchableOpacity containerStyle={styles.btn}>
            <Text>
              Share <Entypo name="share" size={16} color="black" />
            </Text>
          </TouchableOpacity>
        </View>
      </View>
      {state.comment ? (
        <Comment
          postId={data.id}
          userId={auth.user.userId}
          toggleComment={toggleComment}
        />
      ) : null}

      <View style={{ padding: 20 }}>
        <CommentList
          navigation={navigation}
          postId={data.id}
          onMorePressed={onMorePressed}
        />
      </View>
      
      <Modal
        animated={true}
        onRequestClose={closeModal}
        visible={state.modalVisible}>
        <AllComments
          closeModal={closeModal}
          navigation={navigation}
          postId={data.id}
        />
      </Modal>
      
      <Modal
        animated={true}
        style={{marginTop: 60}}
        onRequestClose={closeLikerModal}
        visible={state.likerModalVisible}>
        <UserList
          closeModal={closeLikerModal}
          navigation={navigation}
          userIds={state.likeUserIds}
        />
      </Modal>
      
      </View>
      </SafeAreaView>
  );
};
export default FeedCard;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingVertical: 10,
    borderBottomWidth: 1,
    borderColor: '#f4f4f4',
  },

  userInfo: {
    flexDirection: 'row',
    height: 50,
    alignItems: 'center',
    paddingHorizontal: 10,
    marginBottom: 5,
  },

  user_image: {
    width: 40,
    height: 40,
    borderRadius: 20,
    backgroundColor: '#ccc',
    overflow: 'hidden',
  },
  userName: {
    marginLeft: 10,
    fontSize: 13,
    fontWeight: 'bold',
  },
  imageContainer: {
    height: height / 2.5,
    backgroundColor: '#f4f4f4',
  },
  image: {
    width: null,
    height: null,
    flex: 1,
    resizeMode: 'cover',
  },

  description: {
    padding: 10,
  },
  descriptionText: {
    fontSize: 16,
  },

  actionContainer: {
    flexDirection: 'row',
    height: 50,
  },
  btn: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#f4f4f4',
    flexDirection: 'row',
  },

  likesContainer: {
    padding: 10,
    marginBottom: 10,
    width: 100,
  },
  likeText: {
    fontSize: 14,
    fontWeight: 'bold',
  },
  likeCommentBtn: {
    flex: 1,
    backgroundColor: '#f8f8f8',
  },
});
