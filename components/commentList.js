import React from 'react';
import { View, Text, StyleSheet, Image, SafeAreaView } from 'react-native';
import { TouchableOpacity, FlatList } from 'react-native-gesture-handler';
import Reply from './commentReply';
import firestore from '@react-native-firebase/firestore';
import auth from '@react-native-firebase/auth';
import { getUserDetailsById } from '../utils';
import Comment from './comment';
import moment from 'moment';
import ReplyBox from './ReplyBox';

class CommentList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      comments: [],
      reply: [],
      loggedInId: '',
      commentsCount: 0,
    };
  }
  toggleReply = commentId => {
    let { reply } = this.state;
    if (!reply[commentId]) {
      reply[commentId] = true;
    } else {
      reply[commentId] = false;
    }
    this.setState({ reply });
  };
  componentDidMount = async () => {
    const { postId } = this.props;
    const loggedInId = await auth().currentUser.uid;
    this.setState({ loggedInId });

    let limit = 2;
    firestore()
      .collection('comments')
      .where('postId', '==', postId)
      .orderBy('createdAt', 'DESC')
      .limit(limit)
      .onSnapshot(async snapshot => {
        if (!snapshot.empty) {
          let { comments } = this.state;
          snapshot._docs.forEach(doc => {
            let {
              id,
              text,
              userId,
              userName,
              profile,
              replyIds,
              recentReply,
              createdAt,
            } = doc.data();
            //console.log('createdAt', createdAt);
            if (!createdAt) {
              createdAt = { seconds: new Date().getTime() / 1000 };
            }
            if (
              comments &&
              comments.length > 0 &&
              comments.some(comment => comment.id === id)
            ) {
              //existing comment
              let index = comments.indexOf(
                comments.filter(comment => comment.id === id)[0],
              );
              comments[index].replyIds = replyIds;
              comments[index].recentReply = recentReply;
            } else {
              if (comments.length === limit) {
                comments.shift();
              }
              comments.push({
                id,
                userId,
                text,
                replyIds,
                recentReply,
                userName,
                profilePic: profile,
                replyVisible: false,
                createdAt,
              });
            }
            //console.log('comments', comments);
            this.setState({ comments: comments.reverse() });
          });
        }
      });
    //     this.setState({
    //       comments: comments.reverse(),
    //     });
    //   });
    // });
    // const res = await firestore()
    //   .collection('comments')
    //   .where('postId', '==', postId)
    //   .get();
    // if (!res.empty) {
    //   this.setState({commentsCount: res._docs.length});
    // }

    firestore()
      .collection('posts')
      .doc(postId)
      .onSnapshot(snapshot => {
        const { commentIds } = snapshot.data();
        if (commentIds) {
          this.setState({ commentsCount: commentIds.length });
        } else {
          this.setState({ commentsCount: 0 });
        }
      });
  };

  renderComments = item => {
    const props = this.props;
    const { reply, loggedInId } = this.state;
    const { recentReply } = item;
    const { navigation } = this.props;
    // console.log('userDetails', userDetails);
    // console.log('replyIds', item.item.replyIds);
    return (
      <>
      <SafeAreaView style={{flex:1,justifyContent:'flex-start', paddingTop: 20  }}>
        <View style={{ ...styles.container, ...props.myStyles }}>
          <TouchableOpacity
            onPress={() =>
              navigation.navigate('FriendsProfile', {
                profile: {
                  fullname: item.userName,
                  profile: item.profilePic,
                  noButton: true,
                },
              })
            }
            style={styles.avatar}>
            <Image source={{ uri: item.profilePic }} style={styles.avatar} />
          </TouchableOpacity>
          <View style={styles.commentInfo}>
            <TouchableOpacity
              onPress={() =>
                navigation.navigate('FriendsProfile', {
                  profile: {
                    fullname: item.userName,
                    profile: item.profilePic,
                    noButton: true,
                  },
                })
              }>
              <Text style={styles.username}>{item.userName}</Text>
            </TouchableOpacity>
            <Text style={styles.commentText}>{item.text}</Text>
            {item.createdAt && (
              <Text style={styles.commentInfo}>
                {moment(new Date(item.createdAt.seconds * 1000)).fromNow()}
              </Text>
            )}
            <View
              style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
              <TouchableOpacity onPress={() => this.toggleReply(item.id)}>
                <Text
                  style={{
                    fontSize: 14,
                    fontWeight: 'bold',
                    marginHorizontal: 5,
                  }}>
                  Reply
                </Text>
              </TouchableOpacity>
              {item.replyIds && item.replyIds.length > 0 && (
                <Text style={{ marginRight: 40, fontWeight: 'bold' }}>
                  {'Replies: ' + item.replyIds.length}
                </Text>
              )}
            </View>
          </View>
        </View>
        {reply[item.id] === true ? (
          <ReplyBox
            commentId={item.id}
            userId={loggedInId}
            toggleReply={this.toggleReply}
          />
        ) : null}
        {recentReply && (
          <View style={styles.replyContainer}>
            <TouchableOpacity
              onPress={() =>
                navigation.navigate('FriendsProfile', {
                  profile: {
                    fullname: recentReply.userName,
                    profile: recentReply.profilePic,
                    noButton: true,
                  },
                })
              }
              style={styles.avatar}>
              <Image
                source={{
                  uri: recentReply.profilePic,
                }}
                style={styles.avatar}
              />
            </TouchableOpacity>
            <View style={styles.commentInfo}>
              <TouchableOpacity
                onPress={() =>
                  navigation.navigate('FriendsProfile', {
                    profile: {
                      fullname: recentReply.userName,
                      profile: recentReply.profilePic,
                      noButton: true,
                    },
                  })
                }>
                <Text style={styles.username}>{recentReply.userName}</Text>
              </TouchableOpacity>
              <Text style={styles.commentText}>{recentReply.reply}</Text>
              {recentReply.createdAt && (
                <Text style={styles.commentInfo}>
                  {moment(
                    new Date(recentReply.createdAt.seconds * 1000),
                  ).fromNow()}
                </Text>
              )}
            </View>
          </View>
        )}
        {/* <Reply commentId={item.id} /> */}
        {/* {item.replyIds.map(replyId => {
          return <Reply replyId={replyId} />;
        })} */}
        </SafeAreaView>
      </>
    );
  };

  render() {
    const { comments, commentsCount } = this.state;
    const { onMorePressed } = this.props;
    return (
      <>
      <SafeAreaView style={{flex:1,justifyContent:'flex-start', paddingTop: 20  }}>

        <Text style={{ fontWeight: 'bold', marginBottom: 5 }}>
          {'Comments: ' + commentsCount}
        </Text>
        {comments.map(comment => {
          return this.renderComments(comment);
        })}
        {commentsCount > 0 && (
          <TouchableOpacity onPress={onMorePressed}>
            <Text style={{ fontWeight: 'bold' }}>More</Text>
          </TouchableOpacity>
        )}
        {/* <FlatList
          data={comments}
          renderItem={this.renderComments}
          keyExtractor={(item, index) => String(index)}
        /> */}
      </SafeAreaView>

      </>
    );
  }
}
export default CommentList;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
  },

  avatar: {
    width: 30,
    height: 30,
    borderRadius: 15,
    backgroundColor: '#f8f8f8',
    overflow: 'hidden',
    marginRight: 5,
  },

  replyContainer: {
    paddingHorizontal: 35,
    flexDirection: 'row',
    marginVertical: 5,
  },
  commentInfo: {},
  username: {
    fontSize: 12,
    fontWeight: 'bold',
  },

  commentText: {
    fontSize: 14,
    backgroundColor: '#f8f8f8',
    padding: 10,
  },
});
