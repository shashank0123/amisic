import React from 'react';
import { Picker } from '@react-native-community/picker';

const MyPicker = ({ value, onValueChange, myStyles }) => {
  return (
    <Picker
      onValueChange={onValueChange}
      selectedValue={value}
      style={{ height: 50, width: '100%', ...myStyles }}>
      <Picker.Item label="None" />
      <Picker.Item
        label="What is your pet's name"
        value="What is your pet's name"
      />
      <Picker.Item
        label="What is your mother's maiden name"
        value="What is your mother's maiden name"
      />
    </Picker>
  );
};
export default MyPicker;
