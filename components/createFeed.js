import React, { useState } from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Dimensions,
  PermissionsAndroid,
  Image,
  SafeAreaView,
  Alert,
  Platform,
} from 'react-native';
import Header from './header';
import ImagePicker from 'react-native-image-picker';
import Video from 'react-native-video';
import ImageResizer from 'react-native-image-resizer';

import colors from '../constants/colors';

import { useSelector, useDispatch } from 'react-redux';
import { useNavigation } from '@react-navigation/native';

import Feather from 'react-native-vector-icons/Feather';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Input from './input';
import Button from './button';
import firestore from '@react-native-firebase/firestore';
import storage, { firebase } from '@react-native-firebase/storage';
import { getPosts } from '../store/actions/Home';
import { LOADING, LOADING_FALSE } from '../store/actions/types';

const { height } = Dimensions.get('window');

const options = {
  title: 'Select Image',
  storageOptions: {
    skipBackup: true,
    path: 'images',
    quality: 0.5,
    noData: true,
    maxWidth: 200,
    maxHeight: 200,
  },
};

const CreateFeeds = props => {
  const [state, setState] = useState({
    image: null,
    video: null,
    audio: false,
    description: '',
  });
  const auth = useSelector(state => state.auth);
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const pickImage = async () => {
    const granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
    );
    if (granted !== PermissionsAndroid.RESULTS.GRANTED) {
      return Alert.alert(
        'Insufficiant permissions!',
        'you need to grant camera permission to use this app',
        [{ text: 'Ok' }],
      );
    }

    ImagePicker.launchImageLibrary(options, response => {
      console.log('Imresponse', response);
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else {
        //const source = {uri: response.uri};

        // You can also display the image using data:
        // const source = { uri: 'data:image/jpeg;base64,' + response.data };
        console.log('response', response);
        ImageResizer.createResizedImage(response.uri, 800, 600, 'JPEG', 80)
          .then(({ uri }) => {
            response.uri = uri;
            console.log('resize uri', uri);
            setState({
              ...state,
              image: response,
            });
            // this.setState(
            //   {
            //     resizedImageUri: uri,
            //   },
            //   () => console.log('resizedImageUri', this.state.resizedImageUri),
            // );
          })
          .catch(err => {
            console.log(err);
            return Alert.alert(
              'Unable to resize the photo',
              'Check the console for full the error message',
            );
          });
      }
    });
  };

  const onCreatePost = async () => {
    if (!state.description && !state.image) {
      Alert.alert('Cannot create empty post!');
      return;
    }
    dispatch({ type: LOADING });
    setTimeout(() => dispatch({ type: LOADING_FALSE }), 20000);
    try {
      //const storage = storage();
      if (state.disability && state.disability.trim()) {
        let myPostDisabilities = [];
        let postDisabilities = [];
        let disabilityId;
        myPostDisabilities = state.disability.split(',');

        const results = myPostDisabilities.map(async myDisability => {
          myDisability = myDisability.trim().toLowerCase();

          const res = await firestore()
            .collection('disabilities')
            .where('name', '==', myDisability)
            .get();

          if (!res.empty) {
            // if it is an existing disability
            const data = res._docs[0].data();

            disabilityId = data.id;
          }
          return disabilityId;
        });
        Promise.all(results).then(completed => {
          completed.forEach(item => {
            postDisabilities.push(item);
          });
          const postRef = firestore()
            .collection('posts')
            .doc();
          const file = state.image;
          const videoFile = state.video;
          if (file && file.path) {
            const ext = file.path.split('.').pop(); // Extract image extension
            const currentUser = firebase.auth().currentUser.uid;
            const date = Date.now();
            const filename = `${file.fileName}.${currentUser}.${date}.${ext}`; // Generate unique name
            try {
              storage()
                .ref(`images/${filename}`)
                .putFile(file.path)
                .then(async snapshot => {
                  const myUrl = await storage()
                    .ref(`images/${filename}`)
                    .getDownloadURL();
                  postRef.set({
                    id: postRef.id,
                    userId: auth.user.userId,
                    description: state.description,
                    image: myUrl,
                    createdAt: firestore.FieldValue.serverTimestamp(),
                    disabilityIds: postDisabilities,
                    likeUserIds: [],
                    circleId: props.circleId,
                  });
                  dispatch(getPosts(navigation));
                  //dispatch({ type: LOADING_FALSE });
                  //navigation.navigate('Home');
                });
            } catch (err) {
              console.log('er', err);
            }
          } else if (videoFile) {
            const ext = 'mp4'; // Extract image extension
            const currentUser = firebase.auth().currentUser.uid;
            const date = Date.now();
            const filename = `Video.${currentUser}.${date}.${ext}`; // Generate unique name
            try {
              // storage
              //   .ref(`videos/${filename}`)
              //   .putFile(videoFile.uri)
              //   .then(async snapshot => {
              //     postRef.set({
              //       id: postRef.id,
              //       userId: auth.user.userId,
              //       description: state.description,
              //       video: snapshot.downloadURL,
              //       createdAt: firestore.FieldValue.serverTimestamp(),
              //       disabilityIds: postDisabilities,
              //       likeUserIds: [],
              //       circleId: props.circleId,
              //     });
              //     dispatch(getPosts(navigation));
              dispatch({ type: LOADING_FALSE });
              navigation.navigate('Home');
              //});
            } catch (err) {
              console.log('er', err);
            }
          } else {
            postRef.set({
              id: postRef.id,
              userId: auth.user.userId,
              description: state.description,
              createdAt: firestore.FieldValue.serverTimestamp(),
              // createdAt: firestore.FieldValue.serverTimestamp(),
              disabilityIds: postDisabilities,
              likeUserIds: [],
              circleId: props.circleId,
            });
            dispatch(getPosts(navigation));
          }
        });
      } else {
        const postRef = firestore()
          .collection('posts')
          .doc();
        const file = state.image;
        const videoFile = state.video;
        if (file && file.path) {
          const ext = file.path.split('.').pop(); // Extract image extension
          const currentUser = firebase.auth().currentUser.uid;
          const date = Date.now();
          const filename = `${file.fileName}.${currentUser}.${date}.${ext}`; // Generate unique name
          try {
            storage()
              .ref(`images/${filename}`)
              .putFile(file.path)
              .then(async snapshot => {
                const myUrl = await storage()
                  .ref(`images/${filename}`)
                  .getDownloadURL();
                postRef.set({
                  id: postRef.id,
                  userId: auth.user.userId,
                  description: state.description,
                  image: myUrl,
                  createdAt: firestore.FieldValue.serverTimestamp(),
                  disabilityIds: [],
                  likeUserIds: [],
                  circleId: props.circleId,
                });
                dispatch(getPosts(navigation));
                //dispatch({ type: LOADING_FALSE });
                //navigation.navigate('Home');
              });
          } catch (err) {
            console.log('er', err);
          }
        } else if (videoFile) {
          console.log('videoFile', videoFile);
          const ext = 'mp4'; // Extract image extension
          const currentUser = firebase.auth().currentUser.uid;
          const date = Date.now();
          const filename = `Video.${currentUser}.${date}.${ext}`; // Generate unique name
          try {
            // storage
            //   .ref(`videos/${filename}`)
            //   .putFile(videoFile.uri)
            //   .then(async snapshot => {
            //     postRef.set({
            //       id: postRef.id,
            //       userId: auth.user.userId,
            //       description: state.description,
            //       video: snapshot.downloadURL,
            //       createdAt: firestore.FieldValue.serverTimestamp(),
            //       disabilityIds: [],
            //       likeUserIds: [],
            //       circleId: props.circleId,
            //     });
            //     dispatch(getPosts(navigation));
            dispatch({ type: LOADING_FALSE });
            navigation.navigate('Home');
            //});
          } catch (err) {
            console.log('er', err);
          }
        } else {
          postRef.set({
            id: postRef.id,
            userId: auth.user.userId,
            description: state.description,
            createdAt: firestore.FieldValue.serverTimestamp(),
            disabilityIds: [],
            likeUserIds: [],
            circleId: props.circleId,
          });
          dispatch(getPosts(navigation));
        }
      }
    } catch (err) {
      console.log('err', err);
      Alert.alert('Could not create Post!');
    }
  };

  const getVideoPickerResponse = response => {
    console.log('comes here and response', response);
    //console.log('resss', JSON.stringify(response));
    // const source =
    //   Platform.OS === 'android' ? response.path.trim() : response.uri.trim();
    // console.log('source', source);
    setState({
      ...state,
      video: response,
    });
    setTimeout(() => {
      console.log('........', state);
    }, 1000);
    // const { setParentState } = this.props;
    // setParentState({ loading: true });
    // if (response.didCancel) {
    //   Sentry.captureMessage(
    //     'Video Picker Cancelled' + JSON.stringify(response)
    //   );
    //   setParentState({ loading: false });
    // } else if (response.error) {
    //   setParentState({ loading: false });
    //   Sentry.captureMessage('Video Picker Error' + JSON.stringify(response));
    //   if (
    //     Platform.OS === 'ios' &&
    //     response.error.trim() === 'Photo library permissions not granted'
    //   ) {
    //     check('photo').then((response) => {
    //       if (response !== 'authorized') {
    //         this.promptToSettings('photo');
    //       }
    //     });
    //   } else if (
    //     Platform.OS === 'ios' &&
    //     response.error.trim() === 'Camera permissions not granted'
    //   ) {
    //     check('camera').then((response) => {
    //       if (response !== 'authorized') {
    //         this.promptToSettings('camera');
    //       }
    //     });
    //   }
    // } else if (response.customButton) {
    //   setParentState({ loading: false });
    //   Sentry.captureMessage(
    //     'Video Picker Custom button' + JSON.stringify(response)
    //   );
    // } else {
    //   const source =
    //     Platform.OS === 'android' ? response.path.trim() : response.uri.trim();
    //   Sentry.captureMessage('Video Picker URI' + JSON.stringify(response));
    //   RNThumbnail.get(source).then((result) => {
    //     this.setState({ thumbnail: result.path });
    //     setParentState({
    //       thumbnail: result.path
    //     });
    //   });
  };

  const pickVideo = () => {
    //this.setState({ showSpinUp: false });
    console.log('pick called');
    const options = {
      title: 'Select Avatar',
      mediaType: 'video',
      videoQuality: 'high',
      maxWidth: 1280,
      maxHeight: 720,
      storageOptions: {
        skipBackup: true,
        path: 'videos',
        waitUntilSaved: true,
      },
    };
    ImagePicker.launchImageLibrary(options, getVideoPickerResponse);
  };

  const onChangeFeedText = description => {
    setState({ ...state, description });
  };

  const onDisabilityChange = disability => {
    setState({ ...state, disability });
  };

  return (
    <>
    <SafeAreaView style={{flex:1,justifyContent:'flex-start' }}>
      <View style={styles.container}>
        <Header title="Create Feed" />

        {state.video && state.video.uri ? (
          <View style={styles.mediaContainer}>
            <Video
              resizeMode="cover"
              source={{ uri: state.video.uri }}
              style={{ width: '100%', height: 225 }}
              // ref={ref => {
              //   this.player = ref;
              // }}
              controls={true}
            />
          </View>
        ) : null}
        {state.image && state.image.uri ? (
          <View style={styles.mediaContainer}>
            <Image
              style={{ flex: 1, width: null, height: null }}
              source={{ uri: state.image.uri }}
            />
          </View>
        ) : null}
        <Input
          multiline={true}
          myStyles={{
            height:
              (state.image && state.image.uri) ||
                (state.video && state.video.uri)
                ? 70
                : 150,
            borderRadius: 10,
            margin: 5,
            borderWidth: 1,
            borderColor: 'gray',
          }}
          placeholder="What's on your mind?"
          value={state.description}
          onChangeText={onChangeFeedText}
        />
        <View style={{ padding: 5 }}>
          <Text style={styles.label}>Tag Disabilities</Text>
          <Input
            myStyles={{ marginRight: 80 }}
            placeholder="Enter disability here.."
            value={state.disability}
            onChangeText={onDisabilityChange}
          />
        </View>


        <Button
          loading={auth.loading}
          loadingColor="white"
          onPress={onCreatePost}
          title="Post"
          myStyles={{
            backgroundColor: colors.theme,
            width: 120,
            alignSelf: 'center',
            marginTop: 10,
          }}
        />
        <Button
          loading={auth.loading}
          loadingColor="white"
          onPress={props.closeModal}
          title="Close"
          myStyles={{
            backgroundColor: colors.theme,
            width: 120,
            alignSelf: 'center',
            marginTop: 10,
          }}
        />
        <TouchableOpacity onPress={pickImage} style={styles.position1}>
          <View style={styles.addBtn}>
            <Feather name="image" size={20} color="white" />
          </View>
          <Text style={styles.shortText}>Add Image</Text>
        </TouchableOpacity>
        {/* <TouchableOpacity onPress={pickVideo} style={styles.position2}>
          <View style={styles.addBtn}>
            <Feather name="video" size={20} color="white" />
          </View>
          <Text style={styles.shortText}>Add Video</Text>
        </TouchableOpacity> */}
        {/* <TouchableOpacity style={styles.position3}>
          <View style={styles.addBtn}>
            <FontAwesome name="file-audio-o" size={20} color="white" />
          </View>
          <Text style={styles.shortText}>Add Audio</Text>
        </TouchableOpacity> */}
      </View>
      </SafeAreaView>
    </>
  );
};
export default CreateFeeds;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },

  position1: {
    position: 'absolute',
    bottom: 10,
    right: 8,
    alignItems: 'center',
  },
  position2: {
    position: 'absolute',
    bottom: 85,
    right: 8,
    alignItems: 'center',
  },
  position3: {
    position: 'absolute',
    bottom: 160,
    right: 8,
    alignItems: 'center',
  },
  shortText: {
    fontSize: 10,
    fontWeight: 'bold',
  },

  addBtn: {
    width: 50,
    height: 50,
    borderRadius: 25,
    backgroundColor: colors.theme,
    justifyContent: 'center',
    alignItems: 'center',
  },

  addBtnText: {
    color: 'white',
    fontSize: 15,
  },

  mediaContainer: {
    height: height / 3.4,
    backgroundColor: '#f8f8f8',
  },

  label: {
    fontSize: 15,
    fontWeight: 'bold',
    marginLeft: 5,
    marginBottom: 5,
  },
});
