import React from 'react';
import { View, Text, StyleSheet, Dimensions, SafeAreaView } from 'react-native';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import colors from '../constants/colors';
// import moment from 'moment';

const { width } = Dimensions.get('window');

const Message = props => {
  return (
    <>
    <SafeAreaView style={{flex:1,justifyContent:'flex-start' }}>
      <View
        style={{
          ...styles.message,
          ...props.style,
          backgroundColor: props.me ? colors.theme : '#f1f1f1',
          alignSelf: props.me ? 'flex-end' : 'flex-start',
          borderTopLeftRadius: !props.me ? 20 : 20,
          borderTopRightRadius: !props.me ? 20 : 20,
          borderBottomRightRadius: !props.me ? 20 : 0,
          borderBottomLeftRadius: !props.me ? 0 : 20,
        }}>
        <Text
          style={{ ...styles.messageText, color: props.me ? 'white' : 'black' }}>
          {props.message}
        </Text>
        <Text style={{ fontSize: 9 }}>
          {props.messageTime ? props.messageTime : null}
        </Text>
      </View>
      {!props.me ? (
        <View
          style={{
            alignSelf: 'flex-start',
            paddingLeft: 10,
          }}>
          {/* <Text style={{ fontSize: 11 }}>{moment(Date.now()).fromNow()}</Text> */}
        </View>
      ) : null}

      {/* {props.me && props.seen ? (
        <View
          style={{alignSelf: 'flex-end', paddingRight: 10, marginBottom: 5}}>
          <FontAwesome5
            name="check-double"
            color={props.seen ? '0099ff' : 'grey'}
            size={13}
          />
        </View>
      ) : null}
      {props.me ? (
        <View
          style={{alignSelf: 'flex-end', paddingRight: 10, marginBottom: 5}}>
          <FontAwesome5
            name="check"
            color={props.seen ? '0099ff' : 'grey'}
            size={13}
          />
        </View>
      ) : null} */}
      </SafeAreaView >
    </>
  );
};
export default Message;

const styles = StyleSheet.create({
  message: {
    padding: 8,
    maxWidth: width / 1.5 - 10,
    marginHorizontal: 5,
    marginBottom: 10,
  },
  messageText: {
    color: 'white',
    fontSize: 16,
  },
});
