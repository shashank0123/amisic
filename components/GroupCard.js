import React, { useState } from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  Dimensions,
  Alert,
  Modal,
  TouchableOpacity,
} from 'react-native';
import {
  deleteCircle,
  getCircleSuggestions,
  getCurrentUserGroup,
} from '../store/actions/groups';
import { useDispatch } from 'react-redux';
import { useNavigation } from '@react-navigation/native';
import EditCircle from './editCircle';
import Button from './button';
import colors from '../constants/colors';
import firestore from '@react-native-firebase/firestore';
import auth from '@react-native-firebase/auth';

const { width, height } = Dimensions.get('window');

const GroupCard = ({ data, circle, clearState }) => {
  const [state, setState] = useState({
    more: 30,
    edit: false,
    modalData: null,
  });

  //console.log('data>>>>>>>>>>>>>>>', data);

  const dispatch = useDispatch();
  const navigation = useNavigation();

  const onDeleteGroup = data => {
    Alert.alert('Confirmation', 'Are you sure you want to delete or edit?', [
      { text: 'Cancel' },
      { text: 'Edit', onPress: () => onEdit(data.key, data) },
      {
        text: 'Delete',
        onPress: () => {
          dispatch(deleteCircle(data.key, clearState));
        },
      },
    ]);
  };

  const onEdit = (key, data) => {
    setState({ ...state, modalData: data, key, edit: true });
  };

  const navigateTo = (route, data) => {
    //navigation.setParams({circle: data});
    if (data.closeModal) {
      data.closeModal();
    }
    navigation.navigate(route, { circle: data });

  };

  const closeModal = () => {
    setState({ edit: false, modalData: null, key: null });
  };

  const onJoinPress = async id => {
    //console.log('id>>>>', id);
    const userId = auth().currentUser.uid;
    await firestore()
      .collection('circles')
      .doc(id)
      .update({
        userIds: firestore.FieldValue.arrayUnion(userId),
      });
    dispatch(getCircleSuggestions());
    dispatch(getCurrentUserGroup());
    clearState();
  };

  return (
    <>
      <Modal
        animationType="fade"
        visible={state.edit}
        onRequestClose={closeModal}>
        <EditCircle
          closeModal={closeModal}
          data={{ ...state.modalData, clearState }}
        />
      </Modal>
      <TouchableOpacity
        onPress={() => navigateTo('Circle-Detail', data)}
        onLongPress={() => (circle ? false : onDeleteGroup(data))}>
        <View style={circle ? styles.bigContainer : styles.container}>
          <View
            style={circle ? styles.bigImageContainer : styles.imageContainer}>
            <Image
              source={{ uri: data.profile }}
              style={{ width: null, height: null, flex: 1 }}
            />
          </View>

          <View style={styles.title}>
            <Text numberOfLines={1} style={styles.titleText}>
              {data.name.toUpperCase()}
            </Text>
          </View>

          {/* <View style={styles.range}>
            <Text style={styles.titleText}>Range {data.range}</Text>
          </View> */}
          <View style={styles.description}>
            <Text style={styles.titleText}>Description</Text>
            <Text numberOfLines={3} style={styles.descriptionText}>
              {data.description.trim()}
            </Text>
          </View>
          {circle && (
            <Button
              myStyles={{
                position: 'absolute',
                bottom: 10,
                width: '78%',
                backgroundColor: colors.theme,
              }}
              onPress={() => onJoinPress(data.id)}
              //onPress={() => dispatch(logout(navigation))}
              title="Join"
            />
          )}
        </View>
      </TouchableOpacity>
    </>
  );
};
export default GroupCard;

const styles = StyleSheet.create({
  container: {
    width: width / 2 - 10,
    height: 300,
    margin: 5,
    backgroundColor: '#f4f4f4',
    borderRadius: 8,
    borderColor: 'gray',
    padding: 2,
    borderWidth: 1,
  },
  bigContainer: {
    width: width / 2 - 10,
    height: 340,
    margin: 5,
    backgroundColor: '#f4f4f4',
    borderRadius: 8,
    borderColor: 'gray',
    padding: 2,
    borderWidth: 1,
  },

  imageContainer: {
    height: '55%',
    backgroundColor: '#f4f4f4',
    overflow: 'hidden',
  },

  bigImageContainer: {
    height: '46%',
    backgroundColor: '#f4f4f4',
    overflow: 'hidden',
  },

  description: {
    paddingHorizontal: 10,
  },

  descriptionText: {
    fontSize: 13,
  },

  title: {
    padding: 10,
  },

  titleText: {
    fontSize: 14,
    fontWeight: 'bold',
  },

  range: {
    paddingHorizontal: 10,
  },
});
