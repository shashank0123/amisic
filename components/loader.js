import React from 'react';
import { ActivityIndicator } from 'react-native';

const loader = ({ loadingColor }) => {
  return <ActivityIndicator color={loadingColor} size="small" />;
};
export default loader;
