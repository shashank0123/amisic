import React, { useState, useEffect } from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
  Modal,
  Alert,
  SafeAreaView,
  StatusBar,
  PermissionsAndroid,
} from 'react-native';
import Header from './header';
import { ScrollView } from 'react-native-gesture-handler';
import Input from './input';
import Picker from './Picker';
import Button from './button';
import ChangePassword from './changePassword';
import { useSelector, useDispatch } from 'react-redux';
import { updateProfile, clearLoader } from '../store/actions/Auth';
import { useNavigation } from '@react-navigation/native';
import ImageResizer from 'react-native-image-resizer';

const options = {
  title: 'Select Image',
  storageOptions: {
    skipBackup: true,
    path: 'images',
    quality: 0.5,
    noData: true,
    maxWidth: 200,
    maxHeight: 200,
  },
};

import ImagePicker from 'react-native-image-picker';
import colors from '../constants/colors';

const ProfileEdit = ({ saveProfile, closeProfileModal }) => {
  const auth = useSelector(state => state.auth);
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const [state, setState] = useState({
    fullname: auth.user ? auth.user.fullname : '',
    // securityQuestion: auth.user ? auth.user.securityQuestion : '',
    // securityAns: auth.user ? auth.user.securityAns : '',
    isDisabled: auth.user.isDisabled,
    userDisabilities: auth.user.userDisabilities,
    changePassword: false,
    profile: auth.user.profile ? auth.user.profile : '',
  });

  const onChangeUserName = text => {
    setState({ ...state, fullname: text });
  };

  const onPickerChange = value => {
    setState({ ...state, securityQuestion: value });
  };

  const onChangePickerValue = text => {
    setState({ ...state, securityAns: text });
  };

  const closeModal = () => {
    setState({ ...state, changePassword: false });
  };

  const openModal = () => {
    setState({ ...state, changePassword: !state.changePassword });
  };

  const onSave = () => {
    const { fullname, securityQuestion, securityAns, profile } = state;

    if (!fullname) {
      return Alert.alert('Sorry!', 'Fullname is required');
    }
    // if (!securityQuestion) {
    //   return Alert.alert('Sorry!', 'Security Question is required');
    // }
    // if (!securityAns) {
    //   return Alert.alert('Sorry!', 'Security Answer is required');
    // }

    // if (!profile) {
    //   return Alert.alert('Sorry!', 'Profile Picture is required');
    // }

    const data = {
      fullname,
      securityQuestion,
      securityAns,
    };
    // setState({...state, changePassword: false});
    dispatch(
      updateProfile(
        data,
        auth.user.key,
        saveProfile,
        auth.user.userId,
        profile,
        clearState,
      ),
    );
  };

  const clearState = () => {
    setState({ ...state, profile: '' });
  };

  const pickImage = async () => {
    const granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
      PermissionsAndroid.PERMISSIONS.CAMERA,
    );

    if (granted !== PermissionsAndroid.RESULTS.GRANTED) {
      return Alert.alert(
        'Insufficiant permissions!',
        'you need to grant camera permission to use this app',
        [{ text: 'Ok' }],
      );
    }

    ImagePicker.launchImageLibrary(options, response => {
      //console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else {
        //const source = {uri: response.uri};

        // You can also display the image using data:
        // const source = { uri: 'data:image/jpeg;base64,' + response.data };
        ImageResizer.createResizedImage(
          response.uri,
          800,
          600,
          'JPEG',
          80,
        ).then(({ uri }) => {
          response.uri = uri;
          console.log('resize uri', uri);
          setState({
            ...state,
            profile: response,
          });
        });
      }
    });
  };

  useEffect(() => {
    dispatch(clearLoader());
  }, [dispatch]);

  return (
    <>
    <SafeAreaView style={{flex:1,justifyContent:'flex-start' }}>
      <Modal
        animationType="fade"
        animated={true}
        visible={state.changePassword}
        onRequestClose={closeModal}>
        <ChangePassword closeModal={closeModal} />
      </Modal>
      <Header title="Profile Edit" icon={false} />
      <ScrollView pointerEvents={auth.loading ? 'none' : 'auto'}>
        <View style={styles.container}>
          <View style={styles.imageContainer}>
            <Image
              source={{
                uri: state.profile.uri
                  ? state.profile.uri
                  : state.profile
                    ? state.profile
                    : 'https://cdn4.iconfinder.com/data/icons/e-commerce-181/512/477_profile__avatar__man_-512.png',
              }}
              style={{ width: null, height: null, flex: 1, borderRadius: 100 }}
            />
          </View>
          <TouchableOpacity onPress={pickImage} style={{ alignItems: 'center' }}>
            <Text style={styles.chooseProfile}>Choose Profile</Text>
          </TouchableOpacity>
          <View style={styles.list}>
            <Input
              onChangeText={onChangeUserName}
              placeholder="username"
              value={state.fullname}
            />
          </View>
          {state.isDisabled && (
            <View style={[styles.list, { flexDirection: 'row' }]}>
              <Text style={styles.text}>{'Disabilities-  '}</Text>
              {auth.user.userDisabilities.map(disability => {
                return (
                  <Text style={styles.text}>{disability.name + ', '}</Text>
                );
              })}
            </View>
          )}
          {/* <View style={styles.list}>
            <Picker
              value={state.securityQuestion}
              onValueChange={onPickerChange}
            />
          </View>
          <View style={styles.list}>
            <Input
              onChangeText={onChangePickerValue}
              placeholder="security question"
              value={state.securityAns}
            />
          </View> */}

          <Button
            myStyles={{ backgroundColor: colors.theme }}
            onPress={openModal}
            title="Change Password"
          />

          <Button
            loading={auth.loading}
            loadingColor="white"
            onPress={onSave}
            myStyles={{ marginVertical: 20, backgroundColor: colors.theme }}
            title="Save"
          />
          <Button
            onPress={closeProfileModal}
            myStyles={{ marginVertical: 0, backgroundColor: colors.theme }}
            title="Close"
          />
        </View>
      </ScrollView>
      </SafeAreaView >
    </>
  );
};
export default ProfileEdit;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },

  imageContainer: {
    width: 200,
    height: 200,
    borderRadius: 200 / 2,
    backgroundColor: '#f4f4f4',
    alignSelf: 'center',
    marginVertical: 10,
  },

  chooseProfile: {
    fontWeight: 'bold',
    color: 'tomato',
    fontSize: 15,
  },

  list: {
    height: 50,
    marginVertical: 20,
    marginHorizontal: 20,
  },
  text: {
    fontSize: 15,
  },
});
