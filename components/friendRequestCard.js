import React from 'react';
import { View, Text, StyleSheet, Image, Dimensions } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import firestore from '@react-native-firebase/firestore';
import auth from '@react-native-firebase/auth';
import { sendTextMessage } from '../utils';
const { width, height } = Dimensions.get('window');

const FriendRequestCard = props => {
  const onAcceptPress = async () => {
    const uid = auth().currentUser.uid;
    firestore()
      .collection('friendRequests')
      .doc(props.id)
      .delete();

    firestore()
      .collection('users')
      .doc(uid)
      .update({
        friendIds: firestore.FieldValue.arrayUnion(props.fromId),
        incomingRequestIds: firestore.FieldValue.arrayRemove(
          props.fromId,
        ),
      });

    firestore()
      .collection('users')
      .doc(props.fromId)
      .update({
        friendIds: firestore.FieldValue.arrayUnion(uid),
        requestedIds: firestore.FieldValue.arrayRemove(uid),
      });

    sendTextMessage({
      from: props.fromId,
      senderName: props.name,
      senderProfile: props.profile,
      to: uid,
      message: '',
    });
  };

  const onRejectPress = () => {
    const uid = auth().currentUser.uid;
    firestore()
      .collection('friendRequests')
      .doc(props.id)
      .delete();

    firestore()
      .collection('users')
      .doc(uid)
      .update({
        incomingRequestIds: firestore.FieldValue.arrayRemove(
          props.fromId,
        ),
      });

    firestore()
      .collection('users')
      .doc(props.fromId)
      .update({
        requestedIds: firestore.FieldValue.arrayRemove(uid),
      });
  };
  return (
    <View style={styles.container}>
      <View style={{ alignItems: 'center' }}>
        <View style={styles.imageContainer}>
          <Image source={{ uri: props.profile }} style={styles.imageContainer} />
        </View>
        <Text style={styles.username}>{props.name}</Text>
      </View>
      <View style={styles.row}>
        <TouchableOpacity onPress={onAcceptPress} containerStyle={styles.btn}>
          <Text style={styles.accept}>Accept</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={onRejectPress} containerStyle={styles.btn}>
          <Text style={styles.reject}>Reject</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};
export default FriendRequestCard;

const styles = StyleSheet.create({
  container: {
    width: width / 2 - 10,
    height: height / 3.5,
    margin: 5,
    backgroundColor: '#fff',
    borderRadius: 10,
    justifyContent: 'center',
    overflow: 'hidden',
  },

  imageContainer: {
    width: 80,
    height: 80,
    borderRadius: 40,
    backgroundColor: '#f4f4f4',
  },

  row: {
    flexDirection: 'row',
    marginTop: 10,
  },

  btn: {
    flex: 1,
    padding: 5,
    justifyContent: 'center',
    alignItems: 'center',
  },

  accept: {
    color: 'green',
    fontWeight: 'bold',
  },

  reject: {
    color: 'red',
    fontWeight: 'bold',
  },

  username: {
    fontSize: 16,
    fontWeight: 'bold',
    marginTop: 10,
  },
});
