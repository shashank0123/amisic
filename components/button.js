import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import Loader from './loader';

const Button = ({
  title,
  myStyles,
  onPress,
  textStyle,
  loading,
  loadingColor,
}) => {
  return (
    <TouchableOpacity
      onPress={onPress}
      style={{ ...styles.container, ...myStyles }}>
      {loading ? (
        <Loader loadingColor={loadingColor} />
      ) : (
          <Text style={{ ...styles.title, ...textStyle }}>{title}</Text>
        )}
    </TouchableOpacity>
  );
};
export default Button;

const styles = StyleSheet.create({
  container: {
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#f4f4f4',
    marginHorizontal: 20,
    borderRadius: 25,
  },
  title: {
    fontSize: 15,
    color: 'white',
    fontWeight: 'bold',
  },
});
