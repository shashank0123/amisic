import React from 'react';
import { StyleSheet } from 'react-native';
import { TextInput } from 'react-native-gesture-handler';

const Input = ({
  placeholder,
  value,
  onChangeText,
  multiline,
  keyboardType,
  placeholderTextColor,
  secureTextEntry,
  myStyles,
  disabled,
}) => {
  return (
    <TextInput
      secureTextEntry={secureTextEntry}
      style={{ ...styles.container, ...myStyles }}
      placeholder={placeholder}
      value={value}
      onChangeText={onChangeText}
      multiline={multiline}
      keyboardType={keyboardType}
      placeholderTextColor={placeholderTextColor}
      editable={!disabled}
    />
  );
};
export default Input;

const styles = StyleSheet.create({
  container: {
    minHeight: 50,
    backgroundColor: '#f8f8f8',
    borderRadius: 20,
    padding: 5,
    fontSize: 15,
    paddingHorizontal: 10,
  },
});
