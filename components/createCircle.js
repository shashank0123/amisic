import React, { useState, useEffect } from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  SafeAreaView,
  ScrollView,
  Alert,
  TouchableOpacity,
  PermissionsAndroid,
} from 'react-native';
import Header from './header';
import { FlatList } from 'react-native-gesture-handler';
import Input from './input';
import Button from './button';
import { useSelector, useDispatch } from 'react-redux';
import {
  createCircle,
  clearAll,
  getCurrentUsersCreatedGroups,
} from '../store/actions/groups';
import ImagePicker from 'react-native-image-picker';

import Slider from '@react-native-community/slider';
import colors from '../constants/colors';
import strings from '../constants/strings';
import firestore from '@react-native-firebase/firestore';
import auth from '@react-native-firebase/auth';
import GroupCard from './GroupCard';

const options = {
  title: 'Select Image',
  storageOptions: {
    skipBackup: true,
    path: 'images',
    quality: 0.5,
    noData: true,
    maxWidth: 200,
    maxHeight: 200,
  },
};

const CreateCircle = ({ closeModal }) => {
  const [state, setState] = useState({
    name: '',
    image: '',
    description: '',
    range: null,
    suggestionVisible: false,
  });
  const dispatch = useDispatch();
  const auth = useSelector(state => state.auth);
  const onChangeName = text => {
    setState({ ...state, name: text });
  };

  const onChangeDescription = text => {
    setState({ ...state, description: text });
  };

  const clearState = () => {
    setState({ ...state, name: '', image: '', description: '' });
    closeModal();
  };

  const pickImage = async () => {
    const granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
      PermissionsAndroid.PERMISSIONS.CAMERA,
    );

    if (granted !== PermissionsAndroid.RESULTS.GRANTED) {
      return Alert.alert(
        'Insufficiant permissions!',
        'you need to grant camera permission to use this app',
        [{ text: 'Ok' }],
      );
    }

    ImagePicker.launchImageLibrary(options, response => {
      // console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else {
        //const source = {uri: response.uri};

        // You can also display the image using data:
        // const source = { uri: 'data:image/jpeg;base64,' + response.data };

        setState({
          ...state,
          image: response,
        });
      }
    });
  };

  const onCreateCircle = () => {
    const { name, description, image, disabilityType } = state;
    if (!image) {
      return Alert.alert('Sorry!', 'Image is required');
    }
    if (!name) {
      return Alert.alert('Sorry!', 'Circle Name is required');
    }

    if (!description) {
      return Alert.alert('Sorry!', 'Circle Description is required');
    }
    if (!disabilityType) {
      return Alert.alert('Sorry!', 'Disability is required');
    }
    let disabilities = [];
    let disabilityNames = [];
    let myDisabilities = [];
    myDisabilities = disabilityType.split(',');
    console.log('myDisabilities', myDisabilities);

    let empty = true;

    const result = myDisabilities.map(async myDisability => {
      myDisability = myDisability.trim().toLowerCase();
      console.log('myDisability', myDisability);
      const res = await firestore()
        .collection('disabilities')
        .where('name', '==', myDisability)
        .get();

      console.log('res', res);

      if (!res.empty) {
        const data = res._docs[0].data();
        console.log('data', data);
        return {
          id: data.id,
          name: data.name,
        };
      } else {
        return null;
      }
    });

    Promise.all(result).then(completed => {
      completed.forEach(item => {
        if (item) {
          console.log('item', item);
          disabilities.push({
            id: item.id,
            name: item.name,
          });
          disabilityNames.push(item.name);
          empty = false;
        }
      });
      if (!empty) {
        const data = {
          name: name ? name.trim() : '',
          description: description ? description.trim() : '',
          disabilities,
          disabilityNames,
          creatorId: auth.user.userId,
          userIds: [auth.user.userId],
        };

        dispatch(createCircle(data, image, clearState));
        dispatch(getCurrentUsersCreatedGroups());
      } else {
        Alert.alert('The disability you entered is not present!');
      }
    });
  };

  useEffect(() => {
    dispatch(clearAll());
  }, [dispatch]);

  const onChangeDisabilityType = text => {
    setState({ ...state, disabilityType: text });
    //suggestionsPress(text);
  };

  const suggestionsPress = async () => {
    if (state.suggestionVisible) {
      setState({ ...state, suggestionVisible: false });
      return;
    }
    const { disabilityType } = state;
    if (!disabilityType) {
      Alert.alert('Please Enter at least 1 Disability!');
      // setState({
      //   ...state,
      //   //disabilityType,
      //   suggestionVisible: false,
      //   suggestionCircles: [],
      // });
      return;
    }
    let myDisabilities = [];
    myDisabilities = disabilityType.split(',');
    //console.log('myDisabilities', myDisabilities);

    let suggestionCircles = [],
      mySyuggestions = [];
    const results = myDisabilities.map(async myDisability => {
      myDisability = myDisability.trim().toLowerCase();
      const res = await firestore()
        .collection('circles')
        .where('disabilityNames', 'array-contains', myDisability)
        .get();
      return res;
    });

    Promise.all(results).then(completed => {
      completed.forEach(res => {
        res._docs.forEach(doc => {
          if (!doc.data().userIds.includes(auth().currentUser.uid)) {
            suggestionCircles.push(doc.data());
          }
        });
      });
      mySyuggestions = [...new Set(suggestionCircles)];
      if (mySyuggestions && mySyuggestions.length > 0) {
        setState({
          ...state,
          suggestionVisible: true,
          suggestionCircles: mySyuggestions,
        });
      } else {
        setState({
          ...state,
          suggestionVisible: false,
          suggestionCircles: [],
        });
      }
    });
    setState({ ...state, suggestionVisible: true });
  };

  const renderSuggestions = () => {
    const { suggestionCircles } = state;
    //console.log();
    if (!suggestionCircles || suggestionCircles.length < 1) {
      return (
        <Text
          style={{
            textAlign: 'center',
            marginLeft: 5,
            marginBottom: 5,
            color: colors.theme,
            fontWeight: 'bold',
          }}>
          No suggestions found!
        </Text>
      );
    }
    return (
      <View style={styles.suggestionsBox}>
        <Text
          style={{
            textAlign: 'center',
            marginLeft: 5,
            marginBottom: 5,
            color: colors.theme,
            fontWeight: 'bold',
          }}>
          Hey! You may like joining these circles
        </Text>
        <FlatList
          numColumns="2"
          data={suggestionCircles}
          keyExtractor={data => data.key}
          renderItem={data => (
            <GroupCard clearState={clearState} circle={true} data={data.item} />
          )}
        />
      </View>
    );
  };

  return (
  <SafeAreaView style={{flex:1,justifyContent:'flex-start' }}>
    <ScrollView keyboardShouldPersistTaps="always">
      <View
        pointerEvents={auth.loading ? 'none' : 'auto'}
        style={styles.container}>
        <Header title="Create Circle" icon={false} />
        <View style={styles.imageContainer}>
          <Image
            source={{
              uri:
                state.image && state.image.uri
                  ? state.image.uri
                  : 'https://cdn4.iconfinder.com/data/icons/e-commerce-181/512/477_profile__avatar__man_-512.png',
            }}
            style={{ width: null, height: null, flex: 1 }}
          />
        </View>
        <View style={styles.choosePicture}>
          <TouchableOpacity onPress={pickImage}>
            <Text style={styles.choosePictureText}>Choose Picture</Text>
          </TouchableOpacity>
        </View>
        <View>
          <View style={styles.formGroup}>
            <Text style={styles.label}>Circle Name*</Text>
            <Input
              placeholder="Enter circle name here.."
              value={state.name}
              onChangeText={onChangeName}
            />
          </View>
          <View style={styles.formGroup}>
            <Text style={styles.label}>Description*</Text>
            <Input
              multiline={true}
              myStyles={{ minHeight: 70, maxHeight: 90, borderRadius: 10 }}
              placeholder="Enter circle description  here.."
              value={state.description}
              onChangeText={onChangeDescription}
            />
          </View>
          <View style={styles.formGroup}>
            <Text style={styles.label}>Disabilities* {state.range}</Text>
            <Input
              placeholder={strings.disability_type}
              value={state.disabilityType}
              onChangeText={onChangeDisabilityType}
            />
            {/* <Slider
              onValueChange={val =>
                setState({...state, range: Math.round(val)})
              }
              style={{width: '100%', height: 50, flex: 1}}
              minimumValue={0}
              maximumValue={15}
              minimumTrackTintColor="purple"
              maximumTrackTintColor="#000000"
            /> */}
          </View>
          <View style={[styles.formGroup, { flexDirection: 'row' }]}>
            <TouchableOpacity
              onPress={suggestionsPress}
              style={{ marginRight: 40 }}>
              <Text style={styles.buttonText}>See Suggestions</Text>
            </TouchableOpacity>
          </View>
          {state.suggestionVisible && renderSuggestions()}
          <Button
            loading={auth.loading}
            loadingColor="white"
            onPress={onCreateCircle}
            title="Create"
            myStyles={{ backgroundColor: colors.theme, marginBottom: 20 }}
          />
          <Button
            onPress={closeModal}
            title="Close"
            myStyles={{ backgroundColor: colors.theme, marginBottom: 20 }}
          />
        </View>
      </View>
    </ScrollView>
    </SafeAreaView >
  );
};
export default CreateCircle;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },

  imageContainer: {
    width: 200,
    height: 200,
    borderRadius: 100,
    backgroundColor: '#f8f8f8',
    alignSelf: 'center',
    overflow: 'hidden',
  },

  choosePicture: {
    alignItems: 'center',
    marginVertical: 20,
  },
  choosePictureText: {
    fontSize: 16,
    fontWeight: 'bold',
    color: 'tomato',
  },

  label: {
    fontSize: 15,
    fontWeight: 'bold',
    marginLeft: 5,
    marginBottom: 5,
  },

  formGroup: {
    marginHorizontal: 20,
    marginBottom: 10,
  },
  buttonText: {
    color: colors.theme,
    fontWeight: 'bold',
  },
  suggestionsBox: {
    flex: 1,
  },
});
