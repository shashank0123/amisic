import React, { useState } from 'react';
import { View, Text, StyleSheet, Alert, SafeAreaView } from 'react-native';
import Input from './input';
import Button from './button';
import Header from './header';
// import {auth} from 'react-native-firebase';
import { useSelector, useDispatch } from 'react-redux';
import { changePassword } from '../store/actions/Auth';
import colors from '../constants/colors';

const ChangePassword = props => {
  const { closeModal } = props
  const [state, setState] = useState({
    oldPass: '',
    newPass: '',
    confirmPass: '',
  });

  const auth = useSelector(state => state.auth);
  const dispatch = useDispatch();

  const onChangeOldPass = text => {
    setState({ ...state, oldPass: text });
  };

  const onChangeNewPass = text => {
    setState({ ...state, newPass: text });
  };
  const onChangeCnfrmPass = text => {
    setState({ ...state, confirmPass: text });
  };

  const onResetPass = () => {
    const { newPass, confirmPass, oldPass } = state;
    if (!oldPass) {
      return Alert.alert('Invalid', 'Old Password is required');
    }
    if (!confirmPass) {
      return Alert.alert('Invalid', 'Confirm Password is required');
    }
    if (!newPass) {
      return Alert.alert('Invalid', 'New  Password is required');
    }

    if (newPass !== confirmPass) {
      return Alert.alert('Invalid', 'Password is not match');
    }

    dispatch(changePassword(oldPass, newPass));
  };



  return (
    <SafeAreaView style={{flex:1,justifyContent:'flex-start' }}>
    <View
      pointerEvents={auth.loading ? 'none' : 'auto'}
      style={styles.container}>
      <Header title="Change Password" />

      <View style={styles.list}>
        <Input
          onChangeText={onChangeOldPass}
          value={state.oldPass}
          secureTextEntry={true}
          placeholder="Old Password"
        />
      </View>
      <View style={styles.list}>
        <Input
          onChangeText={onChangeNewPass}
          value={state.newPass}
          secureTextEntry={true}
          placeholder="New Password"
        />
      </View>
      <View style={styles.list}>
        <Input
          onChangeText={onChangeCnfrmPass}
          value={state.confirmPass}
          secureTextEntry={true}
          placeholder="Confirm Password"
        />
      </View>
      <Button
        myStyles={{ backgroundColor: colors.theme }}
        onPress={onResetPass}
        loading={auth.loading}
        loadingColor="white"
        title="Reset"
      />
      <Button
        myStyles={{ marginVertical: 20, backgroundColor: colors.theme }}
        onPress={closeModal}
        title="Close"
      />

    </View>
    </SafeAreaView>
  );
};
export default ChangePassword;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  list: {
    height: 50,
    marginVertical: 20,
    marginHorizontal: 20,
  },
});
