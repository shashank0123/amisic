import React from 'react';
import { View, TouchableOpacity, Text, StyleSheet, Image, SafeAreaView } from 'react-native';
import firestore from '@react-native-firebase/firestore';
import auth from '@react-native-firebase/auth';
import ReplyBox from './ReplyBox';
import moment from 'moment';
import Reply from './commentReply';
import { ScrollView } from 'react-native-gesture-handler';
import colors from '../constants/colors';

export default class AllComments extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      comments: [],
      reply: [],
      loggedInId: '',
      commentsCount: 0,
    };
  }

  componentDidMount = async () => {
    const { postId } = this.props;
    const loggedInId = await auth().currentUser.uid;
    this.setState({ loggedInId });

    firestore()
      .collection('comments')
      .where('postId', '==', postId)
      .orderBy('createdAt', 'DESC')
      .onSnapshot(async snapshot => {
        if (!snapshot.empty) {
          let { comments } = this.state;
          snapshot._docs.forEach(doc => {
            const {
              id,
              text,
              userId,
              userName,
              profile,
              replyIds,
              recentReply,
              createdAt,
            } = doc.data();
            //console.log('recentReply', recentReply);
            if (
              comments &&
              comments.length > 0 &&
              comments.some(comment => comment.id === id)
            ) {
              //existing comment
              let index = comments.indexOf(
                comments.filter(comment => comment.id === id)[0],
              );
              comments[index].replyIds = replyIds;
              comments[index].recentReply = recentReply;
            } else {
              //   if (comments.length === limit) {
              //     comments.shift();
              //   }
              comments.push({
                id,
                userId,
                text,
                replyIds,
                recentReply,
                userName,
                profilePic: profile,
                replyVisible: false,
                createdAt,
              });
            }
            this.setState({ comments });
          });
        }
      });
  };

  toggleReply = commentId => {
    let { reply } = this.state;
    if (!reply[commentId]) {
      reply[commentId] = true;
    } else {
      reply[commentId] = false;
    }
    this.setState({ reply });
  };

  renderComments = item => {
    const props = this.props;
    const { navigation, closeModal } = props;
    const { reply, loggedInId } = this.state;
    const { recentReply } = item;
    // console.log('userDetails', userDetails);
    // console.log('replyIds', item.item.replyIds);
    return (
      <>
        <View style={{ ...styles.container, ...props.myStyles }}>
          <TouchableOpacity
            onPress={() => {
              closeModal();
              navigation.navigate('FriendsProfile', {
                profile: {
                  fullname: item.userName,
                  profile: item.profilePic,
                  noButton: true,
                },
              });
            }}
            style={styles.avatar}>
            <Image source={{ uri: item.profilePic }} style={styles.avatar} />
          </TouchableOpacity>
          <View style={styles.commentInfo}>
            <TouchableOpacity
              onPress={() => {
                closeModal();
                navigation.navigate('FriendsProfile', {
                  profile: {
                    fullname: item.userName,
                    profile: item.profilePic,
                    noButton: true,
                  },
                });
              }}>
              <Text style={styles.username}>{item.userName}</Text>
            </TouchableOpacity>
            <Text style={styles.commentText}>{item.text}</Text>
            {item.createdAt && (
              <Text style={styles.commentInfo}>
                {moment(new Date(item.createdAt.seconds * 1000)).fromNow()}
              </Text>
            )}
            <View
              style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
              <TouchableOpacity onPress={() => this.toggleReply(item.id)}>
                <Text
                  style={{
                    fontSize: 14,
                    fontWeight: 'bold',
                    marginHorizontal: 5,
                  }}>
                  Reply
                </Text>
              </TouchableOpacity>
              {item.replyIds && item.replyIds.length > 0 && (
                <Text style={{ marginRight: 40, fontWeight: 'bold' }}>
                  {'Replies: ' + item.replyIds.length}
                </Text>
              )}
            </View>
          </View>
        </View>
        {reply[item.id] === true ? (
          <ReplyBox
            commentId={item.id}
            userId={loggedInId}
            toggleReply={this.toggleReply}
          />
        ) : null}
        {/* {recentReply && (
          <View style={styles.replyContainer}>
            <View style={styles.avatar}>
              <Image
                source={{
                  uri: recentReply.profilePic,
                }}
                style={styles.avatar}
              />
            </View>
            <View style={styles.commentInfo}>
              <Text style={styles.username}>{recentReply.userName}</Text>
              <Text style={styles.commentText}>{recentReply.reply}</Text>
            </View>
          </View>
        )} */}
        {/* <Reply commentId={item.id} /> */}
        {item.replyIds.map(replyId => {
          return (
            <Reply
              closeModal={closeModal}
              navigation={navigation}
              replyId={replyId}
            />
          );
        })}
      </>
    );
  };

  render() {
    const { comments, commentsCount } = this.state;
    const { navigation, closeModal } = this.props;
    return (
      <>
      <SafeAreaView style={{flex:1,justifyContent:'flex-start', paddingTop: 20  }}>
        <View style={{ flexDirection: 'row' }}>
          <View style={{ flex: 1 }}>
            <Text style={{ fontWeight: 'bold', marginVertical: 10, marginLeft: 10 }}>
              {'Comments: '}
            </Text>
          </View>
          <View style={{ flex: 1 }}>
            <TouchableOpacity onPress={closeModal}>
              <View style={{ alignItems: 'flex-end', paddingRight: 10, paddingTop: 10 }}>
                <Text style={{ fontWeight: 'bold', fontSize: 18, color: colors.theme }}>Close</Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>

        {/* <Text style={{ fontWeight: 'bold', marginVertical: 10, marginLeft: 10 }}>
            {'Comments: '}
          </Text> */}
        <ScrollView>
          {comments.map(comment => {
            return this.renderComments(comment);
          })}
        </ScrollView>
        </SafeAreaView >
      </>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    paddingHorizontal: 10,
  },

  avatar: {
    width: 30,
    height: 30,
    borderRadius: 15,
    backgroundColor: '#f8f8f8',
    overflow: 'hidden',
    marginRight: 5,
  },

  replyContainer: {
    paddingHorizontal: 35,
    flexDirection: 'row',
    marginVertical: 5,
  },
  commentInfo: {},
  username: {
    fontSize: 12,
    fontWeight: 'bold',
  },

  commentText: {
    fontSize: 14,
    backgroundColor: '#f8f8f8',
    padding: 10,
  },
});
