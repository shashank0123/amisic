import React from 'react';
import { View, Text, StyleSheet, Image, Alert, SafeAreaView } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { useNavigation } from '@react-navigation/native';

const Message = ({ username, userProfile, text, conversationId, senderId }) => {
  const navigation = useNavigation();
  const navigateTo = (route, name) => {
    navigation.navigate(route, {
      title: name,
      conversationId: conversationId,
      senderId: senderId,
    });
  };

  return (
    <SafeAreaView style={{flex:1,justifyContent:'flex-start' }}>
    <TouchableOpacity onPress={() => navigateTo('Chat', username)}>
      <View style={styles.container}>
        <View style={styles.imageContainer}>
          <Image source={{ uri: userProfile ? userProfile : 'https://cdn4.iconfinder.com/data/icons/e-commerce-181/512/477_profile__avatar__man_-512.png' }} style={styles.imageContainer} />
        </View>
        <View>
          <Text style={styles.username}>{username}</Text>
          {
            text ? <Text style={styles.lastMsg}>{text}</Text> : null
          }

        </View>
      </View>
    </TouchableOpacity>
    </SafeAreaView>
  );
};
export default Message;

const styles = StyleSheet.create({
  container: {
    height: 70,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#fff',
    borderBottomColor: 'rgba(0,0,0,.1)',
    borderBottomWidth: 1,
    paddingHorizontal: 20,
  },
  imageContainer: {
    width: 40,
    height: 40,
    borderRadius: 20,
    backgroundColor: '#f8f8f8',
    marginRight: 10,
  },
  username: {
    fontSize: 14,
    fontWeight: 'bold',
  },
  lastMsg: {
    color: 'grey',
  },
});
