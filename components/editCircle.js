import React, { useState, useEffect } from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  ScrollView,
  Alert,
  TouchableOpacity,
  PermissionsAndroid,
} from 'react-native';
import Header from './header';
import Input from './input';
import Button from './button';
import { useSelector, useDispatch } from 'react-redux';
import { clearAll, updateCircle } from '../store/actions/groups';
import ImagePicker from 'react-native-image-picker';

import Slider from '@react-native-community/slider';
import colors from '../constants/colors';
import { clearLoader } from '../store/actions/Auth';

const options = {
  title: 'Select Image',
  storageOptions: {
    skipBackup: true,
    path: 'images',
    quality: 0.5,
    noData: true,
    maxWidth: 200,
    maxHeight: 200,
  },
};

const EditCircle = ({ data, closeModal }) => {
  const [state, setState] = useState({
    name: data.name,
    image: { uri: data.profile },
    description: data.description,
    range: data.range,
  });

  const dispatch = useDispatch();
  const auth = useSelector(state => state.auth);

  const onChangeName = text => {
    setState({ ...state, name: text });
  };

  const onChangeDescription = text => {
    setState({ ...state, description: text });
  };

  const pickImage = async () => {
    const granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
      PermissionsAndroid.PERMISSIONS.CAMERA,
    );

    if (granted !== PermissionsAndroid.RESULTS.GRANTED) {
      return Alert.alert(
        'Insufficiant permissions!',
        'you need to grant camera permission to use this app',
        [{ text: 'Ok' }],
      );
    }

    ImagePicker.launchImageLibrary(options, response => {
      // console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else {
        //const source = {uri: response.uri};

        // You can also display the image using data:
        // const source = { uri: 'data:image/jpeg;base64,' + response.data };

        setState({
          ...state,
          image: response,
        });
      }
    });
  };

  const onCircleUpdate = () => {
    const { name, description, image } = state;
    if (!image) {
      return Alert.alert('Sorry!', 'Image is required');
    }
    if (!name) {
      return Alert.alert('Sorry!', 'Circle Name is required');
    }

    if (!description) {
      return Alert.alert('Sorry!', 'Circle Description is required');
    }

    const update = {
      name,
      description: description.trim(),
      userId: auth.user.userId,
    };

    if (image && image.uri) {
      return dispatch(updateCircle(data.key, update, closeModal, image));
    }
    dispatch(updateCircle(data.key, update, closeModal, null));
  };

  useEffect(() => {
    dispatch(clearLoader());
  }, [dispatch]);

  return (
    <ScrollView keyboardShouldPersistTaps="always">
      <View
        pointerEvents={auth.loading ? 'none' : 'auto'}
        style={styles.container}>
        <Header title="Edit Circle" icon={false} />
        <View style={styles.imageContainer}>
          <Image
            source={{
              uri: state.image && state.image.uri ? state.image.uri : null,
            }}
            style={{ width: null, height: null, flex: 1 }}
          />
        </View>
        <View style={styles.choosePicture}>
          <TouchableOpacity onPress={pickImage}>
            <Text style={styles.choosePictureText}>Choose Picture</Text>
          </TouchableOpacity>
        </View>
        <View>
          <View style={styles.formGroup}>
            <Text style={styles.label}>Circle Name*</Text>
            <Input
              placeholder="Enter circle name here.."
              value={state.name}
              onChangeText={onChangeName}
            />
          </View>
          <View style={styles.formGroup}>
            <Text style={styles.label}>Description*</Text>
            <Input
              multiline={true}
              myStyles={{ minHeight: 70, borderRadius: 10 }}
              placeholder="Enter circle description  here.."
              value={state.description}
              onChangeText={onChangeDescription}
            />
          </View>

          <Button
            loading={auth.loading}
            loadingColor="white"
            onPress={onCircleUpdate}
            title="Update"
            myStyles={{ backgroundColor: colors.theme, marginBottom: 20 }}
          />
        </View>
      </View>
    </ScrollView>
  );
};
export default EditCircle;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },

  imageContainer: {
    width: 200,
    height: 200,
    borderRadius: 100,
    backgroundColor: '#f8f8f8',
    alignSelf: 'center',
    overflow: 'hidden',
  },

  choosePicture: {
    alignItems: 'center',
    marginVertical: 20,
  },
  choosePictureText: {
    fontSize: 16,
    fontWeight: 'bold',
    color: 'tomato',
  },

  label: {
    fontSize: 15,
    fontWeight: 'bold',
    marginLeft: 5,
    marginBottom: 5,
  },

  formGroup: {
    marginHorizontal: 20,
    marginBottom: 10,
  },
});
