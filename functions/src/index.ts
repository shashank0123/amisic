import * as functions from 'firebase-functions'
import algoliasearch = require('algoliasearch')

const env = functions.config()

const client = algoliasearch(env.algolia.appid, env.algolia.apikey)
const usersIndex = client.initIndex(`users`)
const circlesIndex = client.initIndex(`circles`) // <-- Index name

  export const algoliaCirclesSync = functions
  .firestore.document(`circles/{doc}`).onWrite(async (change, _context) => {
    const oldData = change.before
    const newData = change.after
    const data = newData.data()
    const objectID = newData.id // <-- prop name is important

    if (!oldData.exists && newData.exists) {
        // creating
        return circlesIndex.addObject(Object.assign({}, {
          objectID
        }, data))
      } else if (!newData.exists && oldData.exists) {
        // deleting
        return circlesIndex.deleteObject(objectID)
      } else  {
        // updating
        return circlesIndex.saveObject(Object.assign({}, {
          objectID
        }, data))
    }
})

export const algoliaUsersSync = functions
  .firestore.document(`users/{doc}`).onWrite(async (change, _context) => {
    const oldData = change.before
    const newData = change.after
    const data = newData.data()
    const objectID = newData.id // <---- prop name is important

    if (!oldData.exists && newData.exists) {
      return usersIndex.addObject(Object.assign({}, {
        objectID
      }, data))
    } else if (!newData.exists && oldData.exists) {
      return usersIndex.deleteObject(objectID)
    } else  {
      return usersIndex.saveObject(Object.assign({}, {
        objectID
      }, data))
    }

  })