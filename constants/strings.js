const strings = {
  are_disabled: 'Are you disabled?',
  disability_type: 'Enter disabilities (separated by comma)',
};

export default strings;
