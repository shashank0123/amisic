import 'react-native-gesture-handler';
import React, {useEffect} from 'react';
import {StatusBar, BackHandler} from 'react-native';

//React Navigations - module
import {NavigationContainer} from '@react-navigation/native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {createStackNavigator, TransitionPresets} from '@react-navigation/stack';

//Screens
import Feeds from './screens/Feeds';
import Message from './screens/message';
import Profile from './screens/Profile';
import Friends from './screens/Friend/index';
import Login from './screens/Login';
import Signup from './screens/Signup';
import Groups from './screens/Circle/index';
import CircleDetail from './screens/Circle/Detail';
import FriendProfile from './screens/User';
import Chat from './screens/Chat';

//Icons
import Feather from 'react-native-vector-icons/Feather';
import AntDesign from 'react-native-vector-icons/AntDesign';

//Redux Provider
import store from './store/index';
import {Provider, useSelector} from 'react-redux';

const BottomTab = createBottomTabNavigator();

const BottomTabNav = props => {
  const auth = useSelector(state => state.auth);

  if (auth.user) {
  }

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', () => {
      if (auth.user) {
        return false;
      }
    });
  }, []);

  return (
    <>
      <StatusBar backgroundColor="white" barStyle="dark-content" />
      <BottomTab.Navigator>
        <BottomTab.Screen
          name="Feeds"
          options={{
            tabBarIcon: ({color}) => (
              <Feather name="home" size={24} color={color} />
            ),
          }}
          component={Feeds}
        />
        <BottomTab.Screen
          name="Message"
          options={{
            tabBarIcon: ({color}) => (
              <Feather name="message-square" size={24} color={color} />
            ),
          }}
          component={Message}
        />
        <BottomTab.Screen
          name="Profile"
          options={{
            tabBarIcon: ({color}) => (
              <AntDesign name="profile" size={24} color={color} />
            ),
          }}
          component={Profile}
        />
        <BottomTab.Screen
          name="Circle"
          options={{
            tabBarIcon: ({color}) => (
              <AntDesign name="addusergroup" size={24} color={color} />
            ),
          }}
          component={Groups}
        />
        <BottomTab.Screen
          name="Friends"
          options={{
            tabBarIcon: ({color}) => (
              <Feather name="users" size={24} color={color} />
            ),
          }}
          component={Friends}
        />
      </BottomTab.Navigator>
    </>
  );
};

const Stack = createStackNavigator();

const StackTab = props => {
  return (
    <Stack.Navigator screenOptions={{...TransitionPresets.SlideFromRightIOS}}>
      <Stack.Screen
        name="Login"
        component={Login}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Signup"
        component={Signup}
        options={{headerShown: false}}
      />

      <Stack.Screen
        name="Circle-Detail"
        component={CircleDetail}
        options={{headerShown: false}}
      />

      <Stack.Screen
        name="FriendsProfile"
        component={FriendProfile}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Chat"
        component={Chat}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Home"
        component={BottomTabNav}
        options={{headerShown: false}}
      />
    </Stack.Navigator>
  );
};

const App = () => {
  return (
    <Provider store={store}>
      <NavigationContainer>
        <StatusBar backgroundColor="white" barStyle="dark-content" />

        <StackTab />
      </NavigationContainer>
    </Provider>
  );
};

export default App;
